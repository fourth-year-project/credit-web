# Introduction

This is a angular 7 application that serves as front-end client to [Credit-App](https://gitlab.com/fourth-year-project/credit-app).  

## Get Started

### Prerequisites

Angular cli  version 7

## Installation

```
npm install #never run with sudo

npm start # default port is 4200 consider changing if in use

```

## Deployment

Built as Proof of Concept

## Built With

+ [Nebular](https://akveo.github.io/nebular/)  -  Angular 7 Theming toolkit 

## Version Management

Use Git ,on Gitlab servers 

## Authors

+ **Allen Oyieke** - *work*

  

## License

This project is licensed under the  License - see the [LICENSE](LICENSE.md) 

###  file for details



## Acknowledgments

 + StackOverflow