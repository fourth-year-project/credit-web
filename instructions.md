# Instruction Recap

## Modules
```
ng g module pages/authentication --routing=true

ng g module creditee --routing=true
```

## Components
```
ng g component pages/authentication/login
ng g component pages/authentication/reset-password

```
