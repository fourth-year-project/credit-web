import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MiscellaneousComponent } from './miscellaneous.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';

const routes: Routes = [{
  path: '',
  component: MiscellaneousComponent,
    children: [
      {
        path: '404',
        component: NotFoundComponent,
        data:{}
      },
      {
        path: 'welcome',
        component: WelcomeComponent,
        data:{}
      },
      {
        path: '403',
        component:ForbiddenComponent,
        data:{}
      },
      {
        path: '',
        redirectTo: 'welcome',
        pathMatch: 'full'
      }
],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MiscellaneousRoutingModule { }

export const routedComponents = [
  MiscellaneousComponent,
  NotFoundComponent,
  ForbiddenComponent,
  WelcomeComponent,
];
