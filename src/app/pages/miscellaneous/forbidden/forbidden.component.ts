import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../@core/service/user.service';

@Component({
  selector: 'forbidden',
  templateUrl: './forbidden.component.html',
  styleUrls: ['./forbidden.component.scss']
})
export class ForbiddenComponent implements OnInit {

  constructor(private userService: UserService) {
  }


  ngOnInit() {
  }

  goToHome() {
    this.userService.navigateHome();
  }

}
