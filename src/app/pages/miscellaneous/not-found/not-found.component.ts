import { NbMenuService } from '@nebular/theme';
import { Component } from '@angular/core';
import { UserService } from '../../../@core/service/user.service';

@Component({
  selector: 'ngx-not-found',
  styleUrls: ['./not-found.component.scss'],
  templateUrl: './not-found.component.html',
})
export class NotFoundComponent {

  constructor(private userService: UserService) {
  }

  goToHome() {
    this.userService.navigateHome();
  }
}
