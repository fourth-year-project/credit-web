import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { UserService } from '../../../@core/service/user.service';
import { environment } from '../../../../environments/environment';
import { OrganisationService } from '../../../@core/service/organisation.service';
import { IUser } from '../../../@core/data/users';
import { IOrganisation } from '../../../@core/data/organisation';
import { NbDateService } from '@nebular/theme';
import { map } from 'rxjs/operators';
import { NotificationService } from '../../../@core/utils/notification.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Router } from '@angular/router';

@Component({
  selector: 'registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  registrationForm: FormGroup
  sub_title = ""

  user = <IUser>{}
  organisation = <IOrganisation>{}
  allOrgsList:any[] = []
  allOrgsReady:boolean=false

  min: Date;
  max: Date;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private organisationService:OrganisationService,
    protected dateService: NbDateService<Date>,
    private notificationService:NotificationService,
    private router:Router
    ) {
      let oldest= 365*90
      this.min = this.dateService.addDay(this.dateService.today(), -oldest);
      this.max = this.dateService.today();
    }

  ngOnInit() {

    this.organisationService.getAllOrganisations().subscribe(res=>{

      Object.keys(res).forEach(item=>{
        this.allOrgsList.push(res[item]._registrationNumber)
      })
      console.log("(RegistrationComponent) orgsList:" +JSON.stringify(this.allOrgsList) )

    })

    this.createForm();
  }


  createForm()
  {
    this.organisationService.getAllOrganisations().pipe(map(res=>{


      Object.keys(res).forEach(item=>
        {
          this.allOrgsList.push(res[item]._registrationNumber)
        })
        this.allOrgsReady =true
        console.log("(RegistrationComponent) Done loading orgs list")
    }))

    this.registrationForm = this.fb.group({
        'first_name': new FormControl('',
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(20),
            Validators.pattern(environment.nameRegex)
          ]),
        'last_name': new FormControl('',
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(20),
            Validators.pattern(environment.nameRegex)
          ]),
        'gender': new FormControl('',
          [
            Validators.required,
          ]),
        'phoneNumber': new FormControl('',
          [
            Validators.required,
            Validators.pattern(environment.phoneNumberRegex),
          ]),
        'id_number': new FormControl('',
          [
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(10),
            Validators.pattern(environment.idNumberRegex),
          ]),
        'occupation': new FormControl('',
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(15),
            Validators.pattern(environment.nameRegex),
          ]),
        'email': new FormControl('',
          [
            Validators.required,
            Validators.pattern(environment.emailRegex),
          ]),
        'password': new FormControl('',
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(40),
            Validators.pattern(environment.passwordRegex),
            this.MatchPassword
          ]),
        'password_confirmation': new FormControl('',
          [
            Validators.required,
            this.MatchPassword
          ]),
        'company_registration': new FormControl('',
          [
            Validators.required,
            Validators.minLength(5),
            Validators.pattern(environment.companyRegRegex)
          ]),
        'company_name': new FormControl('',
          [
            Validators.required,
            Validators.minLength(2),
            Validators.pattern(environment.copmanyName)
          ]),
        'company_desc': new FormControl('',
          [
            Validators.required,
            Validators.minLength(4),
            Validators.pattern(environment.sentenceRegex)
          ]),
        'inceptionDate': new FormControl('',[
          Validators.required,

          ]),
        'company_phone_number': new FormControl('',
          [
            Validators.required,
            Validators.minLength(8),
            Validators.pattern(environment.telephoneRegex),
          ]),
        'company_email': new FormControl('',
          [
            Validators.required,
            Validators.pattern(environment.emailRegex),
          ]),
        'location':new FormControl('',[
          Validators.required,

          ]),
        'businessType':new FormControl('',[

        ]),
      })
  }

  MatchPassword(AC: FormControl) {
    let dataForm = AC.parent;

    if(!dataForm) return null;

    let password_confirmation = dataForm.get('password_confirmation').value;
    let password = dataForm.get('password').value;

    if(password != password_confirmation) {
      /* for password_confirmation from current field "password_confirmation" */
      dataForm.controls["password"].setErrors( {MatchPassword: true} );
      if( password == AC ) {
        /* for current field "password" */
        return {password: {MatchPassword: true} };
      }
    } else {
      dataForm.controls["password"].setErrors( null );
    }
    return null;
  }

  companyRegistered(AC: FormControl) {
    let dataForm = AC.parent;
    if(!dataForm) return null;
    dataForm.controls["company_registration"].setErrors( null );
  }


  onSubmit()
  {
      var date = new Date()
      const result: any = Object.assign({}, this.registrationForm.value);


      console.log("(Registration Components): " + JSON.stringify(result ) )

      if( this.allOrgsList.indexOf(result.company_registration) == -1)
      {
        this.organisation._id = result.company_registration
        this.organisation._organisationName = result.company_name
        this.organisation._registrationNumber = result.company_registration
        this.organisation._inceptionDate =  result.inceptionDate
        this.organisation._description =  result.company_desc
        this.organisation._contact = result.company_phone_number + " or " + result.company_email
        this.organisation._location = result.location
        this.organisation._businessType = result.businessType
        this.organisation._inceptionDate = result.inceptionDate
        this.organisation._active =true
        this.organisation._created = date.toISOString()
        this.organisation._modified = date.toISOString()

        this.user._id = result.id_number
        this.user._uuid = result.id_number
        this.user._name = result.first_name + " " +  result.last_name
        this.user._gender = result.gender
        this.user._email = result.email
        this.user._phoneNumber = result.phoneNumber
        this.user._occupation = result.occupation
        this.user._password = result.password
        this.user._active =true
        this.user._account = result.businessType
        this.user._created = date.toISOString()
        this.user._modified = date.toISOString()

        this.organisationService.createOrganisation(this.organisation)

        // this.userService.registerAuthUser(this.user)

        this.userService.register(this.user)

        this.userService.login(this.user._email,this.user._password)
            .subscribe((res)=>{
              console.log("(loginComponent)Login result: "+ JSON.stringify(res) )

              if (this.userService.isAuthenticated())
              {
                console.log("User: autheneticated")

                // Populate user data
                this.userService.getCurrentUserById(this.userService.currentUserValue()._uuid)

                console.log(this.userService.currentUserValue() )

                if (this.userService.isCreditor())
                {
                    window.location.href = '/creditor'
                }
                else if (this.userService.isCreditee())
                {
                  window.location.href = '/creditee'
                }
                else{
                  window.location.href = '/404'
                }
              }
            });
      }
      else
      {
        this.notificationService.makeToast("Registration Service Failed","Company already registered",NbToastStatus.DANGER,true)
        this.registrationForm.controls['company_registration'].setErrors({registrationTaken:true})
      }
  }

}
