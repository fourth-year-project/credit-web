import { NgModule } from '@angular/core';
import { AuthenticationRoutingModule, routedComponents } from './authentication-routing.module';
import { ThemeModule } from '../../@theme/theme.module';

@NgModule({
  imports: [
    ThemeModule,
    AuthenticationRoutingModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class AuthenticationModule {}
