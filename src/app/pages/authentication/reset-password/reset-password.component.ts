import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { UserService } from '../../../@core/service/user.service';
import { IUser } from '../../../@core/data/users';
import { environment } from '../../../../environments/environment';
import { NotificationService } from '../../../@core/utils/notification.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Router } from '@angular/router';

@Component({
  selector: 'reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  resetPasswordForm:FormGroup
  userData:any

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private  notificationService:NotificationService,
    public router: Router
    ) {}

  ngOnInit(): void {
    this.createForm();
  }

  createForm()
  {
    this.resetPasswordForm = this.fb.group({
      'email': new FormControl('',
        [
          Validators.required,
          Validators.pattern(environment.emailRegex),
        ])
      })
  }


  onSubmit()
  {
      const result: any = Object.assign({}, this.resetPasswordForm.value);

      this.userService.getUserByEmail(result.email).subscribe(res=>{
        let tes:any
          console.log("User email fetched: " + JSON.stringify(res) )

          if (res == undefined)
          {
            this.notificationService.makeToast("Account Service","Account does not exist",NbToastStatus.DANGER,true)
          }
          else
          {
            this.notificationService.makeToast("Account Service","Password sent to email",NbToastStatus.SUCCESS,true)
            this.userService.resetPassword(result).subscribe(res=>{
              this.router.navigate(['/login'])
            })

          }
      })
  }
}
