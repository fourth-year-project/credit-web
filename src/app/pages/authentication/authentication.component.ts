import { Component } from '@angular/core';

@Component({
  selector: 'ngx-authentication',
  styleUrls:['authentication.component.scss'],
  template: `
    <nb-layout>
      <nb-layout-column>
        <router-outlet></router-outlet>
      </nb-layout-column>
    </nb-layout>
  `,
})
export class AuthenticationComponent {
}
