import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { UserService } from '../../../@core/service/user.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup
  sub_title = "One for All"
  constructor(
    private fb: FormBuilder,
    private userService: UserService
    )
    {}

  ngOnInit() {
    this.createForm();
  }

  createForm()
  {
    this.loginForm = this.fb.group({
      'email': new FormControl('',
        [
          Validators.required,
          Validators.pattern(environment.emailRegex),
        ]),
        'password': new FormControl('',
          [
            Validators.required,
            Validators.minLength(6),
            Validators.pattern(environment.passwordRegex)
          ]),

      })
  }

  login(email: string, password: string)
  {
    // Call to user service login
    this.userService.login(email, password)
    .subscribe((res)=>{
      console.log("(loginComponent)Login result: "+ JSON.stringify(res) )

      if (this.userService.isAuthenticated())
      {
        console.log("User: autheneticated")

        // Populate user data
        this.userService.getCurrentUserById(this.userService.currentUserValue()._uuid)

        console.log(this.userService.currentUserValue() )

        if (this.userService.isCreditor())
        {
            window.location.href = '/creditor'
        }
        else if (this.userService.isCreditee())
        {
          window.location.href = '/creditee'
        }
        else{
          window.location.href = '/404'
        }
      }

    });

  }

  onSubmit() {
    const result: any = Object.assign({}, this.loginForm.value);

    // Do useful stuff with the gathered data
    console.log("Login form submission: [" + result.email + " "+result.password+ " ]");

    this.login(result.email, result.password)
  }

}
