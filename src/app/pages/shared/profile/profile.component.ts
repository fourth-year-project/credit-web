import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../@core/service/user.service';
import { IUser } from '../../../@core/data/users';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {


  currentUser :any

  constructor( private userService: UserService,
    private router:Router)
    { }

  ngOnInit() {
    this.userService.getUser().subscribe(res=>{
      this.userService.getUserById(res._uuid).subscribe( (usr_res:any)=>{
        this.currentUser = usr_res
      })
    })
    console.log( "ProfileComponent Current user " + JSON.stringify(this.currentUser) )
  }

  //TODO: fix button
  back()
  {
    if ( this.userService.isCreditee() )
    {
      console.log("Is creditee" )
      this.router.navigate([ "/creditee/dsahboard"])
    }
    else if (this.userService.isCreditor())
    {
      this.router.navigate([  environment.appRoutes.creditor +environment.creditorRoutes.dashboard])
    }
    else
    {
      this.router.navigate([ '/login'])
    }
  }
}
