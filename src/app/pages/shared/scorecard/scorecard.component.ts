import { delay } from 'rxjs/operators';
import { AfterViewInit, Component, Input, OnDestroy, OnChanges, SimpleChanges, SimpleChange, TemplateRef, ViewChild } from '@angular/core';
import { NbThemeService, NbPopoverDirective } from '@nebular/theme';

declare const echarts: any;

@Component({
  selector: 'ngx-scorecard',
  styleUrls: ['./scorecard.component.scss'],
  template: `
    <nb-card size="xsmall" class="solar-card"
        [nbPopover]="template"
        nbPopoverTrigger="hover"
        [nbPopoverContext]="_creditScoreExplain"
        nbPopoverPlacement="bottom">
      <nb-card-header>Credit Score</nb-card-header>
      <nb-card-body>
      <div class="clearfix">

        <ngx-charts-pie-grid
          class="chart"
          [view] ="view"
          [scheme]="colorScheme"
          [results]="results"
          [designatedTotal]="100">
        </ngx-charts-pie-grid>
        </div>
        <div class="info">
          <div class="value">{{ _creditScore}}</div>
          <div class="details"><span>out of</span> 100 </div>
        </div>
      </nb-card-body>
    </nb-card>

    <ng-template #template let-data>
      <nb-card class="popover-card">
        <nb-card-body>
          <p>The company was scored in the following category: </p>
          <p> Payment History: {{ data.paymentHistory.explain }}</p>
          <p> Amount Owed: {{ data.amountOwed.explain }}</p>
          <p> Length of Credit History: {{ data.creditHistory.explain }}</p>
          <p> New Credit: {{ data.newCredit.explain }}</p>
          <p> Type of credit used: {{ data.creditType.explain }}</p>
        </nb-card-body>
      </nb-card>
    </ng-template>


  `,
})
export class ScorecardComponent implements OnChanges, OnDestroy {

  private _creditScore:number;
  private results:any
  private _creditScoreExplain:any={};

  @ViewChild(NbPopoverDirective) popover: NbPopoverDirective;

  @Input('creditScoreDataset') creditScoreDataset:any;

  view: any[] = [ 220,200];

  colorScheme = {
    domain: ['#5AA454']
  };
  option: any = {};
  themeSubscription: any;

  constructor(private theme: NbThemeService) {
  }

  ngOnChanges(changes: SimpleChanges) {
    const creditScore: SimpleChange = changes.creditScoreDataset;
    console.log('(ScoreCardComponent) Prev value: ', JSON.stringify (creditScore.previousValue) );
    console.log('(ScoreCardComponent) Current Value:: ', JSON.stringify(creditScore.currentValue) );
    this._creditScore = Number(creditScore.currentValue.score);
    this._creditScoreExplain = creditScore.currentValue.explain

    this.results = [{
      "name": "",
      "value": this._creditScore
    }];

  }

  ngOnDestroy() {
  }
}
