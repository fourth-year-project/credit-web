import { AfterViewInit, Component, OnDestroy, Input, SimpleChanges, SimpleChange } from '@angular/core';
import { NbThemeService } from '@nebular/theme';

@Component({
  selector: 'creditor-dashboard-crediscoredist-bar',
  template: `
    <div echarts [options]="options" class="echart"></div>
  `,
})
export class CreditScoreDistributionComponent implements AfterViewInit, OnDestroy {

  data: any = [];
  options:any={}
  themeSubscription: any;

  xAxis:number[]=[]
  yAxis:number[]=[]

  private _creditScore:number;

  @Input('results') results:any;

  ngOnChanges(changes: SimpleChanges) {
    const results: SimpleChange = changes.results;
    console.log('(ScoreDistributionComponent) Prev value: ', JSON.stringify (results.previousValue) );
    console.log('(ScoreDistributionComponent) Current Value:: ', JSON.stringify(results.currentValue) );

    this.data = results.currentValue


    const unique = (value, index, self) => {
      return self.indexOf(value) === index
    }

    this.count(this.data,this.yAxis)
    // this.xAxis = [...new Set(this.data)]
    this.xAxis = this.data.filter(unique)

  }

  constructor(private theme: NbThemeService) {
  }

  ngAfterViewInit() {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const echarts: any = config.variables.echarts;

      this.options = {
        backgroundColor: echarts.bg,
        color: [colors.primaryLight],
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'shadow',
          },
        },
        grid: {
          left: '3%',
          right: '4%',
          bottom: '3%',
          containLabel: true,
        },
        xAxis: [
          {
            type: 'category',
            data: this.xAxis,
            axisTick: {
              alignWithLabel: true,
            },
            axisLine: {
              lineStyle: {
                color: echarts.axisLineColor,
              },
            },
            axisLabel: {
              textStyle: {
                color: echarts.textColor,
              },
            },
          },
        ],
        yAxis: [
          {
            type: 'value',
            axisLine: {
              lineStyle: {
                color: echarts.axisLineColor,
              },
            },
            splitLine: {
              lineStyle: {
                color: echarts.splitLineColor,
              },
            },
            axisLabel: {
              textStyle: {
                color: echarts.textColor,
              },
            },
          },
        ],
        series: [
          {
            name: 'Score',
            type: 'bar',
            barWidth: '60%',
            data: this.yAxis,
          },
        ],
      };
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }


  count(array_elements:any[],output:any[])
  {
    array_elements.sort();

    var current = null;
    var cnt = 0;
    for (var i = 0; i < array_elements.length; i++) {
        if (array_elements[i] != current) {
            if (cnt > 0) {
                output.push(cnt)
            }
            current = array_elements[i];
            cnt = 1;
        } else {
            cnt++;
        }
    }
    if (cnt > 0) {
      output.push(cnt)
    }
  }
}
