import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScorecardBreakdownComponent } from './scorecard-breakdown.component';

describe('ScorecardBreakdownComponent', () => {
  let component: ScorecardBreakdownComponent;
  let fixture: ComponentFixture<ScorecardBreakdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScorecardBreakdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScorecardBreakdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
