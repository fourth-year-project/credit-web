import { NgModule } from '@angular/core';

import { SharedRoutingModule, routedComponents } from './shared-routing.module';
import { ThemeModule } from '../../@theme/theme.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ScorecardComponent } from './scorecard/scorecard.component';
import { CreditScoreDistributionComponent } from './scorecard/credit-score-distribution.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { ScorecardBreakdownComponent } from './scorecard-breakdown/scorecard-breakdown.component';
import { LoadingScreenComponent } from './loading-screen/loading-screen.component';

@NgModule({
  imports: [
    ThemeModule,
    SharedRoutingModule,
    NgxChartsModule,
    NgxEchartsModule,
  ],
  declarations: [
    ScorecardComponent,
    CreditScoreDistributionComponent,
    ScorecardBreakdownComponent,
    LoadingScreenComponent,
    ...routedComponents,
  ],
  exports: [
    ScorecardComponent,
    CreditScoreDistributionComponent
  ],
})
export class SharedModule { }
