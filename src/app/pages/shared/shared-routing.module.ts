import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { SharedComponent } from './shared.component';
import { environment } from '../../../environments/environment';
import { AuthGuardService } from '../../@core/service/authguard.service';

const routes: Routes = [{
  path: '',
  component: SharedComponent,
  canActivateChild: [AuthGuardService],
  children: [
      {
        path: 'profile',
        component: ProfileComponent,
        data:{allowedRoles:[environment.accountType.creditee,environment.accountType.creditor ]}
      },
],

}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SharedRoutingModule { }

export const routedComponents = [
  SharedComponent,
  ProfileComponent,
];
