
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AnalyticsService } from './@core/utils/analytics.service';
import { Router, NavigationStart, NavigationCancel, NavigationEnd } from '@angular/router';

@Component({
  selector: 'ngx-app',
  template:`
    <div class="router-output">
      <router-outlet></router-outlet>
    </div>
  `,
})
export class AppComponent implements OnInit , AfterViewInit {

    constructor(
      private router: Router
      ) {}


    ngAfterViewInit() {}

    ngOnInit() {}
}
