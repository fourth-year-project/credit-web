import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UtilityService } from '../utils/utility.service';
import { resolve } from 'q';
import { map } from 'rxjs/operators';
import { UserService } from '../service/user.service';

@Injectable()
export class UserConfigResolve implements Resolve<any> {

  constructor(
      private utilService:UtilityService,
      private userService:UserService,
  )
  {}

  resolve(): any| boolean {

    return this.utilService.getAllConfigs().pipe(map(res=>{

      if (res) {
        // console.log("(AllUserConfigResolver) : results " + JSON.stringify(res) )
        return res
      }
      else
      {
          console.log("(AllUserConfigResolver) : error in getting results")
          this.userService.navigateHome()
          return false;
      }

    }))


  }

}
