
import { Injectable } from '@angular/core';
import {Router, Resolve } from '@angular/router';

import { UserService } from '../service/user.service';

@Injectable()
export class UserResolve implements Resolve<any> {
    constructor(
      private userService: UserService,
      private router: Router) { }


    resolve(): any| boolean {
        return this.userService.getAllUsers().map(res=>
        {
            if (res) {
              // console.log("(AllUserResolver) : results " + JSON.stringify(res) )
              return res
            }
            else
            {
                console.log("(AllUserResolver) : error in getting results")
                this.userService.navigateHome()
                return false;
            }
        })
    }
}
