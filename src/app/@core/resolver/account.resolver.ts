
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { AccountService } from '../service/account.service';
import { UserService } from '../service/user.service';

@Injectable()
export class AllAccountsResolver implements Resolve<any> {

    constructor(
      private accountService:AccountService,
      private userService: UserService) { }

    resolve(): any| boolean {
      return this.accountService.getAllAccounts().map( res=>{
          if (res) {
            console.log("(AccountResolver): accounts list sending: " + JSON.stringify(res) )
            return res
        } else {
            this.userService.navigateHome()
            return false;
        }
        })
    }
}


