
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from '../service/user.service';
import { WatchDogService } from '../service/watchdog.service';

@Injectable()
export class AllWatchdogResolve implements Resolve<any> {
    constructor(
      private watchdogService:WatchDogService,
      private userService: UserService) { }

    resolve(): any| boolean {
        return this.watchdogService.getAllWatchDogs().map(res=>{
          if (res) {
                return res
            } else {
                this.userService.navigateHome()
                return false;
            }
        });
    }
}
