
import { Injectable } from '@angular/core';
import {Router,ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { TransactionHistoryService } from '../service/transactionhistory.service';
import { OrganisationService } from '../service/organisation.service';

@Injectable()
export class OrganisationByIDResolve implements Resolve<any> {
    constructor(
      private orgService: OrganisationService,
      private router: Router) { }

    resolve(route: ActivatedRouteSnapshot): any| boolean {
        let id = route.params['id'];
        return this.orgService.getOrganisationByID(id).map(org=> {
            if (org) {
              return org
            } else {
                this.router.navigate(['/dashboard']);
                return false;
            }
        });
    }
}
