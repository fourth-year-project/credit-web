
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { TransactionHistoryService } from '../service/transactionhistory.service';
import { UserService } from '../service/user.service';

@Injectable()
export class AllTransactionHistoryResolve implements Resolve<any> {
    constructor(
      private transService: TransactionHistoryService,
      private userService: UserService) { }

    resolve(): any| boolean {
        return this.transService.getAllTransactionHistory().map(res=>{
          if (res) {
                return res
            } else {
                this.userService.navigateHome()
                return false;
            }
        });
    }
}
