
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { LoanService } from '../service/loans.service';
import { ILoan } from '../data/loan';

@Injectable()
export class AllLoansResolver implements Resolve<any> {

    constructor(
      private loanService:LoanService) { }

    resolve(): any| boolean {
      return  this.loanService.getAllLoans().map( (loans:ILoan[])=>{

          // console.log("(LoanResolver): loans list: " + JSON.stringify(loans) )

          if(loans.length > 0 )
          {
            return loans
          }
          else
          {
            return false
          }
        })
    }
}


