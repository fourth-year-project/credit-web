
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from '../service/user.service';
import { OrganisationService } from '../service/organisation.service';
import { IUser } from '../data/users';

//TODO: fix point of error
@Injectable()
export class CurrentOrganisationResolver implements Resolve<any> {
    constructor(
      private orgService:OrganisationService,
      private userService: UserService) { }

    resolve(): any| boolean {
      this.userService.getUser().subscribe( (user:IUser)=>{
          console.log("(CurrentOrgResolver): gotten user: " + JSON.stringify(user) )

          this.orgService.getCurrentOrganisationByID(user._organisation).subscribe(org_res=>
            {
              console.log("(CurrentOrgResolver) : OrgData" + JSON.stringify(org_res) )
            })
        })
    }
}


