
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from '../service/user.service';
import { OrganisationService } from '../service/organisation.service';

@Injectable()
export class AllOrganisationResolver implements Resolve<any> {
    constructor(
      private orgService:OrganisationService,
      private userService: UserService) { }

    resolve(): any| boolean {
        return this.orgService.getAllOrganisations().map(res=>{
          if (res) {
                return res
            } else {
                this.userService.navigateHome()
                return false;
            }
        });
    }
}
