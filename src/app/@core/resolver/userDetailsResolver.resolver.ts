import { Resolve } from "@angular/router";
import { Injectable } from '@angular/core';
import { CurrentOrganisationResolver } from './currentOrganisation.resolver';
import { CurrentUserResolver } from '../service/currentUser.resolver';

@Injectable()

// TODO: look into possibility
export class UserDetailsResolver implements Resolve<{ foo: any, bar: any }> {
  constructor(
    protected orgResolver: CurrentOrganisationResolver,
    protected userResolver: CurrentUserResolver
  ) {}

  async resolve(route): Promise<{ foo: any, bar: any }> {
    const foo = await this.userResolver.resolve();
    const bar = await this.orgResolver.resolve();

    return { foo, bar };
  }
}
