import { Directive } from '@angular/core';
import { AsyncValidatorFn, AsyncValidator, NG_ASYNC_VALIDATORS, AbstractControl, ValidationErrors } from '@angular/forms';
import { of as observableOf,  Observable} from 'rxjs';
import 'rxjs/add/operator/map';
import { OrganisationService } from '../service/organisation.service';

export function organisationRefNumberNumberValidator(organisationService: OrganisationService): AsyncValidatorFn {
  return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
    return observableOf (organisationService.getAllOrganisations().subscribe(
      res =>
      {
          Object.keys(res).forEach(
              item =>
                {
                    if (res[item]._registrationNumber ==control.value )
                    {
                        return ({"orgRefNumberExists":true})
                    } 
                }) 
                return null
      }
    )
    )}
}

@Directive({
  selector: '[orgRefNumberExists][formControlName],[orgRefNumberExists][formControl],[orgRefNumberExists][ngModel]',
  providers: [{provide: NG_ASYNC_VALIDATORS, useExisting: organisationRefNumberNumberValidatorDirective, multi: true}]
})
export class organisationRefNumberNumberValidatorDirective implements AsyncValidator {
  constructor(private orgService: OrganisationService) {  }

  validate(control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
     return organisationRefNumberNumberValidator(this.orgService)(control);
  }
} 