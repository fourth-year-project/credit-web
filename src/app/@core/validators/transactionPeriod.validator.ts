import { Directive } from '@angular/core';
import { AsyncValidatorFn, AsyncValidator, NG_ASYNC_VALIDATORS, AbstractControl, ValidationErrors } from '@angular/forms';
import { of as observableOf,  Observable} from 'rxjs';
import 'rxjs/add/operator/map';
import { OrganisationService } from '../service/organisation.service';
import { TransactionHistoryService } from '../service/transactionhistory.service';

export function transactionPeriodValidator(): AsyncValidatorFn {
  return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
    
    if ( 
        control.parent.controls['transactionPeriodEnd'].value.valueOf()  > control.parent.controls['transactionPeriodStart'].value.valueOf() 
        &&
        control.parent.controls['transactionPeriodEnd'].value.valueOf()  != control.parent.controls['transactionPeriodStart'].value.valueOf() 
        &&
        (control.parent.controls['transactionPeriodEnd'].value.valueOf()/ (86400000) ) > 1
        &&
        (control.parent.controls['transactionPeriodEnd'].value.valueOf()/ (86400000) ) < 365
        )
        {
            observableOf( {"transPeriodUnacceptable":true})
        }

    return null
  }
}

@Directive({
  selector: '[transPeriodUnacceptable][formControlName],[transPeriodUnacceptable][formControl],[transPeriodUnacceptable][ngModel]',
  providers: [{provide: NG_ASYNC_VALIDATORS, useExisting: organisationRefNumberNumberValidatorDirective, multi: true}]
})

export class organisationRefNumberNumberValidatorDirective implements AsyncValidator {
  constructor(
        private transHistService: TransactionHistoryService,
        private organisationService: OrganisationService
        ) { }
            
  validate(control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
     return transactionPeriodValidator()(control);
  }
}