import { Directive } from '@angular/core';
import { AsyncValidatorFn, AsyncValidator, NG_ASYNC_VALIDATORS, AbstractControl, ValidationErrors, FormGroup, NG_VALIDATORS, Validator, FormControl } from '@angular/forms';
import 'rxjs/add/operator/map';
import { of as observableOf,  Observable} from 'rxjs';
import { type } from 'os';


export function transactionHistoryFiguresValidator(): AsyncValidatorFn {
  return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {

    // if ( control === control.parent.controls['amountIn'])
    // {
    //     // console.log("(transactionHistory): in amountIn :" +  control.parent.controls['amountIn'].value)
    // }
    // else if (  control === control.parent.controls['loanRepayment'])
    // {      // console.log("(transactionHistory): in loanRepayment"  )

    // }

    // console.log("(transactionHistory)" + JSON.stringify (  Object.keys(control.parent.controls)) )

    var total = parseInt(control.parent.controls['savingAmount'].value)+ parseInt(control.parent.controls['oneOffCost'].value) + parseInt(control.parent.controls['loanRepayment'].value) + parseInt(control.parent.controls['recurrentExpenditure'].value)
    console.log("Error params :"+ JSON.stringify(total) )

    var result = total > control.parent.controls['amountIn'].value ?  {"valueAdditionError": true} : null
    console.log("(transactionHistory)" + JSON.stringify (result ) )

    return observableOf(result)

  };
}

@Directive({
  selector: '[amounInInsufficient][formControlName],[amounInInsufficient][formControl],[amounInInsufficient][ngModel]',
  providers: [{provide: NG_ASYNC_VALIDATORS, useExisting: transactionHistoryFiguresValidatorDirective, multi: true}]
})
export class transactionHistoryFiguresValidatorDirective implements AsyncValidator {
  constructor(private fb:FormGroup) {  }

  validate(control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
     return transactionHistoryFiguresValidator()(control);
  }
}

