import { Directive } from '@angular/core';
import { AsyncValidatorFn, AsyncValidator, NG_ASYNC_VALIDATORS, AbstractControl, ValidationErrors } from '@angular/forms';
import { of as observableOf,  Observable} from 'rxjs';
import 'rxjs/add/operator/map';
import { OrganisationService } from '../service/organisation.service';
import { TransactionHistoryService } from '../service/transactionhistory.service';
import { IOrganisation } from '../data/organisation';
import { UserService } from '../service/user.service';
import { IUser } from '../data/users';

export function transactionPeriodStartValidator(transHistService: TransactionHistoryService): AsyncValidatorFn {
  return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
    return observableOf (transHistService.getAllTransactionHistory().subscribe(
      res =>
      {
          var user:IUser = this.userService.getUser()
          Object.keys(res).forEach(
              item =>
                {
                    if (
                        res[item]._id == this.organisationService.getCurrentOrganisationById(user._organisation).subscribe(res =>{return res._id})
                        &&
                        res[item]._transactionPeriodStart ==control.value
                        )
                    {
                        return ({"orgRefNumberExists":true})
                    }
                })
                return null
      }
    )
    )}
}

@Directive({
  selector: '[transPeriodIncludedExists][formControlName],[transPeriodIncludedExists][formControl],[transPeriodIncludedExists][ngModel]',
  providers: [{provide: NG_ASYNC_VALIDATORS, useExisting: organisationRefNumberNumberValidatorDirective, multi: true}]
})
export class organisationRefNumberNumberValidatorDirective implements AsyncValidator {
  constructor(
        private transHistService: TransactionHistoryService,
        private organisationService: OrganisationService,
        private userService:UserService,
        ) { }


  validate(control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
     return transactionPeriodStartValidator(this.transHistService)(control);
  }
}
