import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbAuthModule, NbDummyAuthStrategy } from '@nebular/auth';
import { NbSecurityModule, NbRoleProvider } from '@nebular/security';
import { of as observableOf } from 'rxjs';

import { throwIfAlreadyLoaded } from './module-import-guard';
import {
  AnalyticsService,
  StateService,
} from './utils';
import { ServiceDataModule } from './service/service-data.module';
import { OrganisationData } from './data/organisation';
import { OrganisationService} from './service/organisation.service';
import { UserData } from './data/users';
import { UserService } from './service/user.service';
import { LoanData } from './data/loan';
import { LoanService } from './service/loans.service';
import { TransactionHistoryData } from './data/transactionHistory';
import { TransactionHistoryService } from './service/transactionhistory.service';
import { WatchdogData } from './data/watchdog';
import { WatchDogService } from './service/watchdog.service';
import { HelperService } from './utils/helper.service';
import { CreditScoreService } from './utils/credit-score.service';
import { NotificationService } from './utils/notification.service';
import { AccountService } from './service/account.service';
import { AccountData } from './data/account';

const socialLinks = [
  {
    url: 'https://github.com/',
    target: '_blank',
    icon: 'socicon-github',
  },
  {
    url: 'https://www.facebook.com/',
    target: '_blank',
    icon: 'socicon-facebook',
  },
  {
    url: 'https://twitter.com/',
    target: '_blank',
    icon: 'socicon-twitter',
  },
];

const DATA_SERVICES = [
  {provide: UserData, useClass: UserService},
  {provide: LoanData , useClass:LoanService },
  {provide:TransactionHistoryData , useClass: TransactionHistoryService },
  {provide:WatchdogData , useClass: WatchDogService },
  {provide: OrganisationData, useClass: OrganisationService},
  {provide: LoanData, useClass: LoanService},
  {provide: AccountData, useClass: AccountService}
];

export class NbSimpleRoleProvider extends NbRoleProvider {
  getRole() {
    // here you could provide any role based on any auth flow
    return observableOf('guest');
  }
}

export const NB_CORE_PROVIDERS = [
  ...ServiceDataModule.forRoot().providers,
  ...DATA_SERVICES,
  // ...NbAuthModule.forRoot({

  //   strategies: [
  //     NbDummyAuthStrategy.setup({
  //       name: 'email',
  //       delay: 3000,
  //     }),
  //   ],
  //   forms: {
  //     login: {
  //       socialLinks: socialLinks,
  //     },
  //     register: {
  //       socialLinks: socialLinks,
  //     },
  //   },
  // }).providers,

  NbSecurityModule.forRoot({
    accessControl: {
      guest: {
        view: '*',
      },

      user: {
        parent: 'guest',
        create: '*',
        edit: '*',
        remove: '*',
      },
    },
  }).providers,

  {
    provide: NbRoleProvider, useClass: NbSimpleRoleProvider,
  },

  AnalyticsService,
  StateService,
  HelperService,
  NotificationService,
  CreditScoreService
];

@NgModule({
  imports: [
    CommonModule,
  ],
  exports: [
    NbAuthModule,
  ],
  declarations: [],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }

  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: CoreModule,
      providers: [
        ...NB_CORE_PROVIDERS,
      ],
    };
  }
}
