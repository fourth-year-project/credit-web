import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TransactionHistoryData, ITransactionHistory } from '../data/transactionHistory';
import { environment } from '../../../environments/environment';
import { retry, map } from 'rxjs/operators';
import { of as observableOf,  Observable, config, BehaviorSubject, throwError } from 'rxjs';
import { NotificationService } from '../utils/notification.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';

@Injectable()
export class TransactionHistoryService extends TransactionHistoryData
{
    private transactionHistoryList: ITransactionHistory[]=[];
    private options;

    constructor(private http: HttpClient, public  notificationService:NotificationService)
    {
      super();

      let headers = new HttpHeaders({
        'accept':'application/json',
        'Content-Type': 'application/json'});
      this.options = { headers: headers };
    }

    createTransactionHistory(transactionHistory : ITransactionHistory)
    {
        var data =
        {
            "id": transactionHistory._id,
            "organisationID": transactionHistory._organisationID,
            "amountIn":  transactionHistory._amountIn,
            "recurrentExpenditure":  transactionHistory._recurrentExpenditure,
            "loanRepayment":  transactionHistory._loanRepayment,
            "oneOffCost": transactionHistory._oneOffCost,
            "savingAmount":  transactionHistory._savingAmount,
            "currentDebt":  transactionHistory._currentDebt,
            "transactionPeriodStart":  transactionHistory._transactionPeriodStart,
            "transactionPeriodEnd":  transactionHistory._transactionPeriodEnd,
            "created":  transactionHistory._created,
            "modified":  transactionHistory._modified
        };

        console.log("ITrans  item :" + JSON.stringify(data) )
        return this.http.post<ITransactionHistory>(
            environment.apiEndpoint+environment.apiEndpointUrl.transactionHistorys ,
            JSON.stringify(data),
            this.options)
            .subscribe(
              res => console.log('HTTP response', res),
              err =>
                    {
                      console.log('HTTP Error', err.statusText);

                      // Display Error
                      if(err.statusText == "Created")
                      {
                        this.notificationService.makeToast("Transaction Update","Transaction data successfully stored",NbToastStatus.SUCCESS,true)
                      }
                      else
                      {
                        this.notificationService.makeToast("Transaction Update ","Transaction data failed  to update",NbToastStatus.DANGER,true)
                      }
                    },
              () => console.log('HTTP request completed.')
          );
    }

    getTransactionHistoryByID(orgID: String)
    {

      return this.http.get<ITransactionHistory[]>(
        environment.apiEndpoint+environment.apiEndpointUrl.transactionHistorys+'/'+orgID,
        this.options)
        .subscribe(
          res => console.log('HTTP response', res),
          err =>
                {
                  console.log('HTTP Error', err.statusText);

                  // Display Error
                },
          () => console.log('HTTP request completed.')
      );
    }

    getAllTransactionHistory()
    {
        return this.http.get<ITransactionHistory[]>(
            environment.apiEndpoint + environment.apiEndpointUrl.transactionHistorys,
            this.options)
            .pipe(
              // retry(environment.retryNumber), // retry a failed request up to 3 times
              map( (transactionHistory:any )=>
                  {
                      this.transactionHistoryList.length = 0
                      if (transactionHistory.length < 1)
                      {
                          console.log("transactionHistory empty")
                      }
                      else
                      {
                          for (let i in transactionHistory)
                          {
                              this.transactionHistoryList.push (transactionHistory[i]);
                              // console.log("{TransactionHistoryService} - (getAllTransactionHistory) added item: " + i + " : " + transactionHistory[i]._id )
                          }
                          // console.log("{TransactionHistoryService} - (getAllTransactionHistory) : [list] => " +JSON.stringify(this.transactionHistoryList))
                      }
                      // this.notificationService.makeToast(" Service","Transaction data refreshed",NbToastStatus.SUCCESS,true)
                      return this.transactionHistoryList
                  }
              )
          )
      }
      getTransactionHistoryList() : Observable<ITransactionHistory[]>
      {
          this.getAllTransactionHistory()
          return observableOf(this.transactionHistoryList)
      }


}
