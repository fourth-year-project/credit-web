import { Injectable } from '@angular/core';
import { IOrganisation, OrganisationData, } from '../data/organisation';
import { environment } from '../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { retry, map } from 'rxjs/operators';
import { of as observableOf,  Observable} from 'rxjs';
import { NotificationService } from '../utils/notification.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';

@Injectable()
export class OrganisationService  extends OrganisationData
{
    private options;
    private currentOrganisation : IOrganisation;
    private organisationsList : IOrganisation[] = [];

    constructor( private http:HttpClient, public  notificationService:NotificationService)
    {
        super()

        let headers = new HttpHeaders({
            'accept':'application/json',
            'Content-Type': 'application/json'});
        this.options = { headers: headers };
    }

    createOrganisation(org: IOrganisation)
    {
        var data =
        {
            "id": org._id,
            "organisationName": org._organisationName,
            "registrationNumber": org._registrationNumber,
            "inceptionDate": org._inceptionDate,
            "description": org._description,
            "contact": org._contact,
            "location": org._location,
            "businessType": org._businessType,
            "active": org._active,
            "created": org._created,
            "modified": org._modified
        };

        return this.http.post(
                    environment.apiEndpoint+environment.apiEndpointUrl.organisation,
                    JSON.stringify(data),
                    this.options)
                    .pipe(
                        // retry(environment.retryNumber), // retry a failed request up to 3 times
                        map(org =>
                            {
                               return org;
                            }
                        ))
                        .subscribe(
                            res => console.log('HTTP response', res),
                            err =>
                            {
                              console.log('HTTP Error', err.statusText);

                              // Display Error

                              if(err.statusText == "Created")
                              {
                                this.notificationService.makeToast("Organisation Registration","Successful",NbToastStatus.SUCCESS,true)
                                this.getCurrentOrganisation(data.id)
                              }
                            },
                      () => console.log('HTTP request completed.')
                  );
    }

    getAllOrganisations()
    {
        this.organisationsList.length = 0 // empty list

        return  this.http.get<IOrganisation[]>(
                    environment.apiEndpoint + environment.apiEndpointUrl.organisation,
                    this.options)
                    .pipe(
                        // retry(environment.retryNumber), // retry a failed request up to 3 times
                        map( (organisations :any)=>
                            {
                                if (organisations.length < 1)
                                {
                                    console.log("Orgs empty")
                                }
                                else
                                {
                                    // console.log("Orgs content" + JSON.stringify (organisations))
                                    for (let i in organisations)
                                    {
                                        // console.log("(getAllOrganisations) Org ["+ i +"] : "+ JSON.stringify (organisations[i]) )
                                        this.organisationsList.push (organisations[i]);
                                        // console.log("{OrganisationService} - (getAllOrganisations):[list] : " + JSON.stringify(this.organisationsList))
                                    }
                                }

                                // this.notificationService.makeToast("Service","Organisations Data updated",NbToastStatus.SUCCESS,true)
                                return organisations
                            }
                        )
                    )
    }

    getOrganisationByID(id: String)
    {
        var mid =id.split('/').join('%2F')
        return this.http.get<IOrganisation>(
                    environment.apiEndpoint + environment.apiEndpointUrl.organisation + '/' + mid ,
                    this.options)
                    .pipe(map((organisation:any) =>
                            {
                                return organisation;
                            }
                    )
                )
    }

    getCurrentOrganisation(id: String)
    {
      var mid =id.split('/').join('%2F')
      return this.http.get<IOrganisation>(
                    environment.apiEndpoint + environment.apiEndpointUrl.organisation + "/"  + mid,
                    this.options)
                    .pipe(
                        // retry(environment.retryNumber),// retry a failed request up to 3 times
                        map( (organisation) =>
                            {
                                this.currentOrganisation = organisation[0]

                                // console.log("OrgService Current org: "+JSON.stringify(this.currentOrganisation) )
                                // this.notificationService.makeToast("Service Update","Current organisation data updated",NbToastStatus.SUCCESS,true)
                                return this.currentOrganisation
                            }
                    ))
    }

    getCurrentOrganisationByID(id:string)
    {
        var mid =id.split('/').join('%2F')

        return this.http.get<IOrganisation>(
                    environment.apiEndpoint + environment.apiEndpointUrl.organisation + "/"+  mid)
                    .pipe(
                        map( (organisation:IOrganisation) =>
                            {
                                this.currentOrganisation = organisation

                                console.log("OrgService Current org: "+JSON.stringify(this.currentOrganisation) )
                                // this.notificationService.makeToast("Service Update","Current organisation data updated",NbToastStatus.SUCCESS,true)
                                return this.currentOrganisation
                            }
                    ))

    }

    updateOrganisation( org :IOrganisation)
    {
        return this.http.post<IOrganisation>(environment.apiEndpoint + environment.apiEndpointUrl.updateOrganisation,org, this.options)
    }

    getOrganisationsList() : Observable<IOrganisation[]>
    {
        return observableOf(this.organisationsList)
    }

    getOrganisationDetails (): Observable<IOrganisation>
    {
        return observableOf( this.currentOrganisation)
    }
}
