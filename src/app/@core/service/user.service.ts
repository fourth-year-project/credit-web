import { of as observableOf,  Observable, config, BehaviorSubject, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { UserData, IUser } from '../data/users';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { catchError, map, retry } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NotificationService } from '../utils/notification.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';

@Injectable()
export class UserService extends UserData {

    public currentUserSubject: BehaviorSubject<IUser>;
    public currentUser: Observable<IUser>;

    private usersList : IUser[] =[];
    private employee : IUser[] = [];
    private options;

    private isRegistratedBC:boolean;

    constructor(private http: HttpClient,public router: Router, public  notificationService:NotificationService)
    {
      super();

      let headers = new HttpHeaders({
        'accept':'application/json',
        'Content-Type': 'application/json'});
      this.options = { headers: headers };

      this.currentUserSubject = new BehaviorSubject<IUser>(JSON.parse(localStorage.getItem('currentUser')));
      this.currentUser = this.currentUserSubject.asObservable();
    }

    //
    // Authetication Functions
    //

    isAuthenticated(): boolean {

      if (this.currentUserSubject.value != null && this.currentUserSubject.value._auth_authenticated )
      {
        return true;
      }
      return false;
    }

    isAuthorized(allowedRoles: string[]): boolean {

      if (this.isAuthenticated())
      {
        // check if the list of allowed roles is empty, if empty, authorize the user to access the page
        if (allowedRoles == null || allowedRoles.length === 0) {
          return true;
        }

        // check if the user roles is in the list of allowed roles, return true if allowed and false if not allowed
        return ( allowedRoles.includes(this.currentUserSubject.value._account) );
      }

      this.router.navigate(['/login'])

    }

    isCreditor(): boolean {

      if (this.currentUserSubject.value._account == environment.accountType.creditor )
      {
        return true;
      }
      return false;
    }

    isCreditee(): boolean {

      if (this.currentUserSubject.value._account == environment.accountType.creditee )
      {
        return true;
      }
      return false;
    }

    navigateHome()
    {
      if ( this.isAuthenticated())
      {
          if (this.isCreditee() )
          {
            this.router.navigate(['/creditee/dashboard']);
          }
          else if (this.isCreditor())
          {
            this.router.navigate(['/creditor/dashboard']);
          }
          else
          {
            this.notificationService.makeToast("Service","Not allowed",NbToastStatus.WARNING,true)
            this.router.navigate(['/403']);
          }
      }
      else
      {
          this.notificationService.makeToast("Service","Kindly login first",NbToastStatus.DANGER,true)
          this.router.navigate(['/login']);
      }
    }


    //
    // API Call
    //

    login(email: string, password: string) {
      return this.http.get<any>(environment.apiAuthEndpoint+'users?email='+email+'&password='+ password)
          .pipe(
              retry(environment.retryNumber), // retry a failed request up to 3 times
              catchError(this.handleError),
              map(users =>
              {
                if( users.length == 1)
                {
                  let user = users[0];
                  // login successful
                  if (user.email === email.toLowerCase() && user.password === password )
                  {
                      console.log("UserService: credentials verified")
                      console.log(user)

                      // store user details in local storage to keep user logged in between page refreshes
                      let info = <IUser>{};
                      info._email = user.email;
                      info._password = user.password
                      info._account = user.account
                      info._uuid = user.uuid
                      info._auth_id = user.id
                      info._auth_authenticated = true;
                      localStorage.setItem('currentUser', JSON.stringify(info));
                      this.currentUserSubject.next(info);

                      this.notificationService.makeToast("User Login","Successful",NbToastStatus.SUCCESS,true)

                      return info;
                  }
                  // login unsucessful
                  this.notificationService.makeToast("User Login"," Unsuccessful",NbToastStatus.DANGER,true)

                  return (user)
                }

                return users
                // return this.checkContent(user.email, "user email");
              })
          )

      }

    logout() {

        // TODO: update logout param clear
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');

        var epty:IUser
        this.currentUserSubject.next(epty)

        // this.router.navigate(['/login'])
        this.navigateHome()
    }

    register(user: IUser)
    {

      var allAuthUsersList =this.getAllAuthUser()

      let count = 0
      Object.keys(allAuthUsersList).forEach(item=>
        {count= count +1})

      user._uuid  = "U_" .concat( (count+3).toString() ) ;
      console.log("New user ID : " + user._id  )

      // Register user
      this.registerUser(user)

      // Login User
      this.login(user._email, user._password)

    }

    // Store on blockchain
    registerUser(user: IUser)
    {
      var password = user._password
      var data =
      {
        "id": user._id,
        "uuid": user._uuid,
        "name": user._name,
        "gender": user._gender,
        "email": user._email,
        "phoneNumber": user._phoneNumber,
        "account": user._account,
        "organisation": user._organisation,
        "occupation": user._occupation,
        "active": user._active,
        "created": user._created,
        "modified": user._modified
      }

      console.log("new User: " + JSON.stringify (data))

      return this.http.post(
                  environment.apiEndpoint+environment.apiEndpointUrl.user ,
                  JSON.stringify(data),
                  this.options)
                  .pipe (
                    // retry(environment.retryNumber), // retry a failed request up to 3 times
                    map( res =>
                      {
                        console.log("User login responce :" + JSON.stringify(res) )
                        return
                      }
                    )
                  )
                  .subscribe(
                    res => console.log('HTTP response', res),
                    err =>
                          {
                            console.log('HTTP Error', err.statusText);

                            // Display Error
                            if(err.statusText == "Created")
                            {

                              // Send to mock server
                              let auth_user = <IUser>{};

                              auth_user._uuid = data.uuid,
                              auth_user._email = data.email,
                              auth_user._password = password,
                              auth_user._account = data.account,
                              console.log("Auth User Reg: " +  JSON.stringify(auth_user) )

                              this.registerAuthUser(auth_user)

                            }
                            else
                            {
                              this.notificationService.makeToast("User Registration","Unsuccessful",NbToastStatus.DANGER,true)
                            }
                          },
                    () => console.log('HTTP request completed.')
                );


    }

    // Store email and password to db.json for auth
    registerAuthUser(user: any)
    {
      console.log( "Auth reg init:" + JSON.stringify(user) )
      return this.http.post(
                  environment.apiAuthEndpoint + environment.apiAuthEndpointUrl.createUser,
                  {
                    "uuid":user._uuid,
                    "email":user._email,
                    "password":user._password,
                    "account":user._account,
                  })
                  .subscribe(
                    data  => {
                      console.log("Auth user results:"  + JSON.stringify(data)  )
                      this.notificationService.makeToast("User Account Registration","Successful",NbToastStatus.SUCCESS,true)
                    },
                    error  => {
                      this.notificationService.makeToast("User Service","User auth registration failed. Try again",NbToastStatus.DANGER,true)
                });
    }

    updateAuthPassword(user: any)
    {
      return this.http.patch(
                  environment.apiAuthEndpoint +environment.apiAuthEndpointUrl.patchUser +user._id,
                  {
                    "password":user._password
                  },
                  this.options)
                  .subscribe(
                    data  => {
                      console.log("(UserService): updateAuthPassword response: " +JSON.stringify(data) )
                      this.notificationService.makeToast("UserService","Password updated",NbToastStatus.WARNING,true)
                    },
                    error  => {
                      var err_msg = "Error: [" + error + "] in updating user password"
                      this.notificationService.makeToast("Config Service",err_msg,NbToastStatus.SUCCESS,true)
                });
    }

    updateAuthUserEmail(user: any)
    {
      return this.http.patch(
                  environment.apiAuthEndpoint + environment.apiAuthEndpointUrl.patchUser +user._id,
                  {
                    "email":user._email
                  })
                  .subscribe(
                    data  => {
                      console.log("(UserService): updateAuthUserEmail response: " +JSON.stringify(data) )
                      this.notificationService.makeToast("User Service"," Update user email",NbToastStatus.SUCCESS,true)
                    },
                    error  => {
                      var err_msg = "Error: [" + error + "] in updating configuration for user"
                      this.notificationService.makeToast("Config Service",err_msg,NbToastStatus.SUCCESS,true)
                });
    }

    getUserAuthById(id: number) {
      return this.http.get(environment.apiAuthEndpoint+environment.apiAuthEndpointUrl.getUser + id);
    }

    getAllAuthUser()
    {
      return this.http.get(environment.apiAuthEndpoint+environment.apiAuthEndpointUrl.createUser);
    }

    getUserById(id: string) {
      return this.http.get<IUser>(environment.apiEndpoint+environment.apiEndpointUrl.user +'/'+ id)
                  .pipe(map( (user:IUser) => {
                    console.log("(UserService) userbyID: " + JSON.stringify(user) )
                  return user
                  }));
    }

    getUserByEmail(email: string) {
      return this.http.get<IUser>(environment.apiAuthEndpoint+environment.apiAuthEndpointUrl.createUser +'?email='+ email)
                  .pipe(map( (user:IUser) => {
                    console.log("(UserService) userbyEmail: " + JSON.stringify(user) )
                  return user
                  }));
    }

    getCurrentUserById(id: string) {

      return this.http.get<IUser>(environment.apiEndpoint+environment.apiEndpointUrl.user +'/'+ id)
                  .pipe(map( (user:IUser) => {
                    this.currentUserSubject.value._account = user._account;
                    this.currentUserSubject.value._active = user._active;
                    this.currentUserSubject.value._created = user._created;
                    this.currentUserSubject.value._gender = user._gender;
                    this.currentUserSubject.value._id = user._id;
                    this.currentUserSubject.value._modified = user._modified;
                    this.currentUserSubject.value._name = user._name;
                    this.currentUserSubject.value._occupation = user._occupation;
                    this.currentUserSubject.value._organisation= user._organisation;
                    this.currentUserSubject.value._phoneNumber = user._phoneNumber
                    this.currentUserSubject.value._uuid = user._uuid;

                    return this.currentUserSubject.value
                  }));
    }

    getAllUsers()
    {
      return this.http.get<IUser[]>(
                      environment.apiEndpoint + environment.apiEndpointUrl.user,
                      this.options)
                      .pipe(
                        map( (users:any) =>
                          {
                            this.usersList.length = 0

                            if( users.length < 1 )
                            {
                              console.log("Users empty")
                            }
                            else
                            {
                              for (let i in users)
                              {
                                  // console.log("UserService: user added => " + JSON.stringify(users[i]) )
                                  this.usersList.push (users[i]);
                              }
                            }
                            return  users
                          })
                      )
    }

    resetPassword(user:any)
    {
      return this.http.post<any>('http://localhost:3010/sendResetPassEmail', user)

    }

    closeAccount( id:string , auth_id:string)
    {
        var data =
        {
          "id": id,
          "active": false
        };
        return this.http.post(
            environment.apiEndpoint + environment.apiEndpointUrl.closeUser,
            JSON.stringify(data),
            this.options)
            .subscribe(
              res => console.log('HTTP response', res),
              err =>
                    {
                      console.log('HTTP Error', err.statusText);
                      // Display Error
                      if(err.statusText == "Created")
                      {
                        this.http.delete(environment.apiAuthEndpoint + environment.apiAuthEndpointUrl.patchUser + auth_id).map(
                          res=>{
                            this.notificationService.makeToast("Account","Successfully closed",NbToastStatus.WARNING,true)
                          }
                        );
                      }
                    },
              () => console.log('HTTP request completed.')
          );
    }

    currentUserValue(): IUser {
      return this.currentUserSubject.value;
    }

    getUser(): Observable<IUser> {

      if (this.currentUserSubject.value._gender == null)
      {
        this.getCurrentUserById(this.currentUserSubject.value._uuid)
      }
      return this.currentUser;
    };

    getAllUsersList(): Observable<IUser[]>
    {
      this.getAllUsers()
      return observableOf( this.usersList);
    }

    // getEmployees(organisation: string): Observable<IUser[]> {
    //     return observableOf(
    //               this.usersList
    //               .filter(
    //                 employees => {
    //                         let filteredList : IUser[];
    //                         for (let i in employees)
    //                         {
    //                             if (employees[i].organisation.trim().toLowerCase() == organisation.trim().toLowerCase() )
    //                             {
    //                                 filteredList.push (employees[i]);
    //                             }
    //                         }
    //                         return filteredList;

    //                     }));
    // }




    // Helper

    private handleError(error: HttpErrorResponse) {
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        console.error(
          `Backend returned code ${error.status}, ` +
          `body was: ${error.error}`);
      }
      // return an observable with a user-facing error message
      return throwError(
        'Something bad happened; please try again later.');
    };

    checkContent(x, name) {
      if (x == null) {
          return (name + ' == null');
      }
      else if (x === null) {
        return (name + ' === null');
      }

      else if (typeof x === 'undefined') {
        return (name + ' is undefined');
      }
      else
      {
        return (name + " contains: " + x)
      }
    }
}
