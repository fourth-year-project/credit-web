import { of as observableOf,  Observable, config, BehaviorSubject, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { WatchdogData, IWatchdog } from '../data/watchdog';
import { retry, map } from 'rxjs/operators';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NotificationService } from '../utils';


@Injectable()
export class WatchDogService extends WatchdogData {

    public watchdogsList: IWatchdog[] = [];

    private options;

    constructor(private http: HttpClient,public  notificationService:NotificationService)
    {
      super();

      let headers = new HttpHeaders({
        'accept':'application/json',
        'Content-Type': 'application/json'});
      this.options = { headers: headers };
    }


    createWatchdog( watchdog: IWatchdog)
    {

        var data =
        {
            "active":true,
            "authorized":false,
            "authorizedOrganization":watchdog._authorizedOrganization,
            "authorizingOrganization":watchdog._authorizingOrganization,
            "created":watchdog._created,
            "id":watchdog._id,
            "modified":watchdog._modified
        };

        return this.http.post<IWatchdog>(
            environment.apiEndpoint + environment.apiEndpointUrl.watchdog,
            JSON.stringify(data),
            this.options)
            .pipe(
                // retry(environment.retryNumber), // retry a failed request up to 3 times
                map(watchdogs =>
                    {
                       return watchdogs;
                    }
                ))
                .subscribe(
                    res => console.log('HTTP response', res),
                    err =>
                    {
                      console.log('HTTP Error', err.statusText);

                      // Display Error

                      if(err.statusText == "Created")
                      {
                        this.notificationService.makeToast("Wacthdog Request","Successful created",NbToastStatus.SUCCESS,true)
                      }
                    },
              () => console.log('HTTP request completed.')
          );
    }

    getAllWatchDogs()
    {

        return this.http.get<IWatchdog[]>(
            environment.apiEndpoint + environment.apiEndpointUrl.watchdog,
            this.options)
            .pipe(
                // retry(environment.retryNumber), // retry a failed request up to 3 times
                map( (watchdogs:any )=>
                    {
                        this.watchdogsList.length = 0

                        if (watchdogs.length < 1)
                        {
                            console.log("Watchdogs empty")
                        }
                        else
                        {

                            for (let i in watchdogs)
                            {
                                this.watchdogsList.push (watchdogs[i]);
                                // console.log("Watchdog added item: "+ i +" : "+watchdogs[i]._authorizingOrganization )
                            }
                            // console.log("{WatchdogService} - (getAllWatchDogs) : [list] => " +JSON.stringify(this.watchdogsList))
                        }
                        return watchdogs
                    }
                )
            )
    }

    authorizeWatchdog( id: string,authorized: boolean)
    {
        var data =
        {
            "id": id,
            "authorized":authorized

        };
        return this.http.post(
            environment.apiEndpoint + environment.apiEndpointUrl.authorizeWatchdog,
            JSON.stringify(data),
            this.options)
            .pipe(
                // retry(environment.retryNumber), // retry a failed request up to 3 times
                map(watchdogs =>
                    {
                       return watchdogs;
                    }
                ))
                .subscribe(
                    res => console.log('HTTP response', res),
                    err =>
                    {
                      console.log('HTTP Error', err.statusText);

                      // Display Error

                      if(err.statusText == "Created")
                      {
                        this.notificationService.makeToast("Watchdog ","Successful approved",NbToastStatus.SUCCESS,true)
                      }
                    },
              () => console.log('HTTP request completed.')
          );
    }

    cancelWatchdogRights(id: string)
    {
        var data =
        {
            "id": id
        };
        return this.http.post<IWatchdog>(
            environment.apiEndpoint + environment.apiEndpointUrl.cancelWatchdog,
            JSON.stringify(data),
            this.options)
            .pipe(
                // retry(environment.retryNumber), // retry a failed request up to 3 times
                map(watchdogs =>
                    {
                       return watchdogs;
                    }
                ))
                .subscribe(
                    res => console.log('HTTP response', res),
                    err =>
                    {
                      console.log('HTTP Error', err.statusText);
                      // Display Error
                      if(err.statusText == "Created")
                      {
                          this.notificationService.makeToast("Watchdog Service","Permission succefully cancelled",NbToastStatus.SUCCESS,true)
                          return true
                      }
                      else
                      {
                        this.notificationService.makeToast("Watchdog Service ","Unsuccesfull attempt to cancel rights ",NbToastStatus.DANGER,true)
                      }

                    },
              () => console.log('HTTP request completed.')
          );
    }

    getWatchdogsList(): Observable<IWatchdog[]>
    {
        this.getAllWatchDogs()
        return observableOf(this.watchdogsList)
    }

}
