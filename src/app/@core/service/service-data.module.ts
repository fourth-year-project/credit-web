import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrganisationService } from './organisation.service';
import { WatchDogService } from './watchdog.service';
import { UserService } from './user.service';
import { AuthGuardService } from './authguard.service';
import { LoanService } from './loans.service';
import { TransactionHistoryService } from './transactionhistory.service';

const SERVICES = [
  OrganisationService,
  WatchDogService,
  UserService,
  AuthGuardService,
  LoanService,
  TransactionHistoryService
];

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class ServiceDataModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: ServiceDataModule,
      providers: [
        ...SERVICES,
      ],
    };
  }
}
