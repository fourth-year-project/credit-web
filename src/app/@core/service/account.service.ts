import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of as observableOf,  Observable, config, BehaviorSubject, throwError, pipe } from 'rxjs';
import { environment } from '../../../environments/environment';
import { map, retry } from 'rxjs/operators';
import { AccountData, IAccount } from '../data/account';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NotificationService } from '../utils/notification.service';
import { Router } from '@angular/router';

@Injectable(
  {providedIn: 'root',
})
export class AccountService extends AccountData
{
    private accountList: IAccount[]=[];
    private options;

    constructor(
      private http: HttpClient,
      public  notificationService:NotificationService,
      private router:Router)
    {
      super();

      let headers = new HttpHeaders({
        'accept':'application/json',
        'Content-Type': 'application/json'});
      this.options = { headers: headers };
    }


    createAccount(account:IAccount)
    {
        var data =
        {
          "id": account._id,
          "holder": account._holder,
          "institution": account._institution,
          "institutionName": account._institutionName,
          "accountType": account._accountType,
          "accountName": account._accountName,
          "accountID": account._accountID,
          "active": true,
          "accountStatus": account._accountStatus,
          "created": account._created,
          "modified": account._modified
        }

        console.log("AccountService  createAccount: " + JSON.stringify(data) )
        return this.http.post<IAccount>(
          environment.apiEndpoint + environment.apiEndpointUrl.account ,
          JSON.stringify(data),
          this.options)
          .subscribe(
            res => console.log('HTTP response', res),
            err =>
                  {
                    console.log('HTTP Error', err.statusText);

                    // Display Message
                    if(err.statusText == "Created")
                    {
                      this.notificationService.makeToast("Account Service","Account data successfully stored",NbToastStatus.SUCCESS,true)
                      this.router.navigate([ environment.appRoutes + environment.crediteeRoutes.account ])
                    }
                  },
            () => console.log('HTTP request completed.')
        );
    }

    closeAccount(id:string)
    {
        var data={
          "id":id
        }

        return this.http.post(
            environment.apiEndpoint + environment.apiEndpointUrl.closeAccount,
            JSON.stringify(data),
            this.options)
            .pipe (
              map( res =>
                {
                  console.log ("Account Closed " + res);
                  return res
                }
              ))
              .subscribe(
                res => console.log('HTTP response', res),
                err =>
                      {
                        console.log('HTTP Error', err.statusText);

                        // Display Error

                        if(err.statusText == "Created")
                        {
                          this.notificationService.makeToast("Account Service","Account closed.Data successfully stored",NbToastStatus.SUCCESS,true)
                        }
                      },
                () => console.log('HTTP request completed.')
            );
    }

    updateAccount(id:string,status:string)
    {
        var data={
          "id":id,
          "accountStatus":status
        }

        return this.http.post(
          environment.apiEndpoint + environment.apiEndpointUrl.updateAccount,
          JSON.stringify(data),
          this.options)
          .pipe (
            map( res =>
              {
                console.log ("Account Updated " + res);
                return res
              }
            ))
            .subscribe(
              res => console.log('HTTP response', res),
              err =>
                    {
                      console.log('HTTP Error', err.statusText);

                      // Display Error
                      if(err.statusText == "Created")
                      {
                        this.notificationService.makeToast("Account Update","Account data successfully updated",NbToastStatus.SUCCESS,true)
                      }
                      else
                      {
                        this.notificationService.makeToast("Account Update","Account data failed updated",NbToastStatus.DANGER,true)
                      }
                    },
              () => console.log('HTTP request completed.')
          );
    }

    getAccountById(acc_id:string)
    {
      return this.http.get<IAccount[]>(
        environment.apiEndpoint+environment.apiEndpointUrl.account+ '/' + acc_id,
        this.options)
        .subscribe(
          res => console.log('HTTP response', res),
          err =>
                {
                  console.log('HTTP Error', err.statusText);
                  // Display Error
                },
          () => console.log('HTTP request completed.')
      );
    }

    getAllAccounts()
    {
        return this.http.get<IAccount[]>(
            environment.apiEndpoint + environment.apiEndpointUrl.account,
            this.options)
            .pipe(
              // retry(environment.retryNumber), // retry a failed request up to 3 times
              map( (account:any )=>
                  {
                      this.accountList.length = 0
                      if (account.length < 1)
                      {
                          console.log("account empty")
                      }
                      else
                      {
                          for (let i in account)
                          {
                              this.accountList.push (account[i]);
                              // console.log("{AccountService} - (getAllAccounts) added item: " + i + " : " + account[i]._id )
                          }
                          // console.log("{AccountService} - (getAllAcccounts) : [list] => " +JSON.stringify(this.accountList))
                      }
                      // this.notificationService.makeToast(" Service","Transaction data refreshed",NbToastStatus.SUCCESS,true)
                      return account
                  }
              )
          )
      }

      getAccountList() : Observable<IAccount[]>
      {
          this.getAllAccounts()
          return observableOf(this.accountList)
      }
}
