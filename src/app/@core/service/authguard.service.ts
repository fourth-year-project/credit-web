import { Injectable } from "@angular/core";
import { UserService } from './user.service';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';
import { NotificationService } from '../utils/notification.service';


@Injectable()
export class AuthGuardService implements CanActivate, CanActivateChild
{
  constructor(public auth: UserService, public router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean
  {
    const allowedRoles = next.data.allowedRoles;
    const isAuthorized = this.auth.isAuthorized(allowedRoles);

    if (!isAuthorized)
    {
        // if not authorized, show access denied message
        this.auth.navigateHome();
    }
    return isAuthorized;
  }

  canActivateChild (
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean
    {
    const allowedRoles = next.data.allowedRoles;
    const isAuthorized = this.auth.isAuthorized(allowedRoles);

    if (!isAuthorized) {
        // if not authorized, show access denied message
        this.auth.navigateHome();
      }
      return isAuthorized;
  }
}
