
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from './user.service';
import { OrganisationService } from './organisation.service';
import { IUser } from '../data/users';

@Injectable()
export class CurrentUserResolver implements Resolve<any> {

  constructor(
      private userService: UserService) {
       }

    resolve(): any| boolean {
        var user :any = this.userService.currentUserValue()
        console.log("(CurrentUserResolver): gotten user: " + JSON.stringify(user) )

        return this.userService.getUserById(user._uuid).subscribe(usr_res=>{

          if (usr_res) {
            console.log("(CurrentUserResolver) : UsrData returned" + JSON.stringify(usr_res) )
            return usr_res
          } else {
            console.log("(CUrrentUserResolver) : error in getting results")
            this.userService.navigateHome()
            return false;
          }

       });
    }
}
