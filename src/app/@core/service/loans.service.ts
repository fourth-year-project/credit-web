import { LoanData, ILoan } from '../data/loan';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of as observableOf,  Observable, config, BehaviorSubject, throwError, pipe } from 'rxjs';
import { environment } from '../../../environments/environment';
import { map, retry } from 'rxjs/operators';
import { NotificationService } from '../utils/notification.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Router } from '@angular/router';

@Injectable()
export class LoanService extends LoanData {

    private LoansList : ILoan[] = []
    private options;

    constructor(private http: HttpClient,
      private notificationService:NotificationService,
      private router:Router)
    {
      super();

      let headers = new HttpHeaders({
        'accept':'application/json',
        'Content-Type': 'application/json'});
      this.options = { headers: headers };
    }

    createLoan( loan : ILoan)
    {
        var data =
        {
          "id": loan._id,
          "issueDate": loan._issueDate,
          "issueConfirmation": false,
          "recepient": loan._recepient,
          "amount": loan._amount,
          "account":loan._account,
          "repaymentPeriod": loan._repaymentPeriod,
          "paymentConfirmation": false,
          "paymentConfirmationDate":loan._paymentConfirmationDate,
          "extensionPeriod":loan._extensionPeriod,
          "extensionStatus":loan._extensionStatus,
          "created": loan._created,
          "modified": loan._modified
        };

        return this.http.post(
                    environment.apiEndpoint + environment.apiEndpointUrl.loan,
                    JSON.stringify(data),
                    this.options)
                    .pipe (
                        map( res =>
                          {
                            console.log ("Loan post " + res);
                            return res
                          }
                        ))
                        .subscribe(
                          res => console.log('HTTP response', res),
                          err =>
                                {
                                  console.log('HTTP Error', err.statusText);

                                  // Display Error

                                  if(err.statusText == "Created")
                                  {
                                    this.notificationService.makeToast("Loan Srvice","Loan record successsfully tored ",NbToastStatus.SUCCESS,true)
                                    this.router.navigate([ environment.appRoutes + environment.crediteeRoutes.loan ])
                                  }
                                },
                          () => console.log('HTTP request completed.')
                      );
    }

    confirmLoan( id: string,issueConfirmation:boolean )
    {
        var data =
        {
          "id": id,
          "issueConfirmation": issueConfirmation
        };

        return this.http.post(
            environment.apiEndpoint + environment.apiEndpointUrl.confirmLoan,
            JSON.stringify(data),
            this.options)
            .subscribe(
              res => console.log('HTTP response', res),
              err =>
                    {
                      console.log('HTTP Error', err.statusText);

                      // Display Error

                      if(err.statusText == "Created")
                      {
                      }
                    },
              () => console.log('HTTP request completed.')
          );
    }
    confirmLoanPayment( id: string,confirmation:boolean)
    {
      var data =
      {
        "id": id,
        "confirmation": confirmation
      };
        return this.http.post(
            environment.apiEndpoint + environment.apiEndpointUrl.confirmLoanPayment,
            JSON.stringify(data),
            this.options)
            .subscribe(
              res => console.log('HTTP response', res),
              err =>
                    {
                      console.log('HTTP Error', err.statusText);

                      // Display Error

                      if(err.statusText == "Created")
                      {

                      }
                    },
              () => console.log('HTTP request completed.')
          );
    }

    getLoanByID(id: String)
    {
        return this.http.get<ILoan>(
            environment.apiEndpoint + environment.apiEndpointUrl.loan + '/' + id,
            this.options);
    }

    getAllLoans()
    {
       return this.http.get<ILoan[]>(
                      environment.apiEndpoint + environment.apiEndpointUrl.loan,
                      this.options)
                      .pipe(
                        map( (loans:any)=>
                        {
                          if (loans.length < 1)
                          {
                              console.log("Orgs empty")
                          }
                          else
                          {
                              // console.log("Orgs content" + JSON.stringify (loans))
                              for (let i in loans)
                              {
                                  // console.log("(getAllloans) Org ["+ i +"] : "+ JSON.stringify (loans[i]) )
                                  this.LoansList.push (loans[i]);
                                  // console.log("{loanservice} - (getAllloans):[list] : " + JSON.stringify(this.loansList))
                              }
                          }

                          // this.notificationService.makeToast("Service","loans Data updated",NbToastStatus.SUCCESS,true)
                          return loans

                        }
                      ))
    }

    // getLoan(name:String) : Observable<ILoan[]>
    // {
    //   return observableOf(
    //     this.getAllLoans()
    //     .filter(
    //       loans => {
    //               let filteredList : ILoan[];
    //               for (let i in loans)
    //               {
    //                   if (loans[i].recepient.trim().toLowerCase() == name.trim().toLowerCase() )
    //                   {
    //                       filteredList.push (loans[i]);
    //                   }
    //               }
    //               return filteredList;

    //           }));
    // }

    getLoanList(): Observable<ILoan[]>
    {
      return observableOf(this.LoansList)
    }

}
