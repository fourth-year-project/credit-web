import { Observable } from 'rxjs';

export interface IWatchdog 
{
    _active: boolean;
    _authorized: boolean;
    _authorizedOrganization: string;
    _authorizingOrganization: string;
    _created: string;
    _id: string;
    _modified: string;
    _type: string;
}
export abstract class WatchdogData 
{
    abstract createWatchdog( watchdog: IWatchdog): any ;
    abstract getAllWatchDogs():any;

    abstract authorizeWatchdog( id: string,authorized: boolean): any;
    abstract cancelWatchdogRights( id: string): any;

    abstract getWatchdogsList(): Observable<IWatchdog[]>;

}