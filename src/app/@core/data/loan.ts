import { Observable } from 'rxjs';

//TODO: update interface
export interface ILoan {
  _account: string;
  _amount: number;
  _created: String;
  _extensionPeriod: number;
  _extensionStatus: boolean;
  _id: string;
  _issueConfirmation: boolean;
  _issueDate: String;
  _modified: String;
  _paymentConfirmation: boolean;
  _paymentConfirmationDate: String;
  _recepient: string;
  _repaymentPeriod: number;
  _type: string;
}

export abstract class LoanData
{
    abstract createLoan( loan : ILoan): any;
    abstract confirmLoan( id: string,issueConfirmation:boolean ): any;
    abstract confirmLoanPayment( id: string,confirmation:boolean)
    abstract getLoanByID(id: String): any;
    abstract getAllLoans(): any

    // abstract getLoans(name:String) : Observable<ILoan[]>
    // abstract getLoanList(): Observable<ILoan[]>
}
