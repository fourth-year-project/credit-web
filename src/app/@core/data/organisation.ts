import { Observable } from 'rxjs';

export interface IOrganisation {
  _active: boolean;
  _businessType: string;
  _contact: string;
  _created: string;
  _description: string;
  _id: string;
  _location: string;
  _modified: string;
  _organisationName: string;
  _registrationNumber: string;
  _inceptionDate: string;
  _type: string;
  _watchdog_authorized?:boolean
  _watchdog_active?:boolean
}

export abstract class OrganisationData {

  abstract createOrganisation(org: IOrganisation): any;
  abstract getAllOrganisations(): any;
  abstract getOrganisationByID(id: String): any;
  abstract getCurrentOrganisationByID(id:string): any
  abstract getCurrentOrganisation(id: String): any;
  abstract updateOrganisation( org :IOrganisation):any;

  abstract getOrganisationsList():  Observable<IOrganisation[]>;
  abstract getOrganisationDetails(): Observable<IOrganisation>;
}
