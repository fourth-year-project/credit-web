import { Observable } from 'rxjs';


export interface ITransactionHistory
{
    _amountIn: number;
    _created: string;
    _currentDebt: number;
    _id?: string;
    _loanRepayment: number;
    _oneOffCost: number;
    _modified: string;
    _organisationID: string;
    _recurrentExpenditure: number;
    _savingAmount: number;
    _transactionPeriodEnd: string;
    _transactionPeriodStart: string;
    _type?: string;
}

export abstract class TransactionHistoryData
{
    abstract createTransactionHistory(transactionHistory : ITransactionHistory): any;
    // abstract updateTransactionHistory(transactionHistory : ITransactionHistory) : any;
    abstract getTransactionHistoryByID(orgID: String):any;
    abstract getAllTransactionHistory():any;

    // abstract getTransactionHistory(name: String): Observable <ITransactionHistory[]>
}

