import { Observable } from 'rxjs';

export interface IAccount {
  _accountID: string;
  _accountName: string;
  _accountStatus: string;
  _accountType: string;
  _active: boolean;
  _created: Date;
  _holder: string;
  _id: string;
  _institution: string;
  _institutionName: string;
  _modified: Date;
  _type: string;
}

export abstract class AccountData
{
  abstract createAccount(account:IAccount):any
  abstract closeAccount(id:string):any
  abstract updateAccount(id:string,status:string):any
  abstract getAccountById(acc_id:string):any
  abstract getAllAccounts():any

  abstract getAccountList() : Observable<IAccount[]>
}
