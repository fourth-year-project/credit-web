import { Observable } from 'rxjs';

export interface IUser {
  _account: string;
  _active: boolean;
  _created: string;
  _email: string;
  _gender: string;
  _id: string;
  _modified: string;
  _name: string;
  _occupation: string;
  _organisation: string;
  _phoneNumber: string;
  _type: string;
  _uuid: string;
  _password?:string;
  _auth_picture?:string;
  _auth_authenticated?:boolean;
  _auth_id?:number;

}

export abstract class UserData
{
  abstract isAuthenticated():boolean;
  abstract isAuthorized(allowedRoles: string[]):boolean;
  abstract isCreditor(): boolean;
  abstract isCreditee():boolean;
  abstract navigateHome();

  abstract login(email:string, password:string):Observable<any>;
  abstract logout();
  abstract register(user: IUser):any;
  abstract registerUser(user: IUser):any;
  // abstract registerAuthUser(user: IUser):any //private

  abstract updateAuthPassword(user: IUser):any;
  abstract updateAuthUserEmail(user: IUser):any;

  abstract getUserById(id: string) : any;
  abstract getCurrentUserById(id: string) : any;
  abstract getUserAuthById(id: number)
  abstract getAllUsers(): any;
  abstract resetPassword(email: string):any;
  abstract closeAccount( id:string , auth_id:string):any;

  abstract currentUserValue(): IUser ;
  abstract getUser(): Observable<IUser>;
  abstract getAllUsersList(): Observable<IUser[]>

  // abstract getEmployees(organisation: string): Observable<IUser[]>
}
