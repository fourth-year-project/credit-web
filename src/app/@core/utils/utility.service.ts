import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { NotificationService } from '../utils/notification.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class UtilityService {

  private options;

  constructor(
    private http: HttpClient,
    public  notificationService:NotificationService,
    private router:Router,
  )
  {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'});
    this.options = { headers: headers };
  }

  createUserConfig(result:any)
  {
      this.http.post("http://localhost:8087/configs",
        {
          "ph":result.ph,
          "ao":result.ao,
          "lch":result.lch,
          "nc":result.nc,
          "tcu":result.tcu,
          "uuid":result.uuid
        })
    .subscribe(
        data  => {
          console.log("(UtilityService): createUserConfig response: " +JSON.stringify(data) )
          this.notificationService.makeToast("Config Service","Created configuration for user",NbToastStatus.SUCCESS,true)
          this.router.navigate(['/creditor/dashboard'])
        },
        error  => {
          var err_msg = "Error: [" + error + "] in creating configuration for user"
          this.notificationService.makeToast("Config Service",err_msg,NbToastStatus.SUCCESS,true)

    });
  }

  getUserConfig(id:string)
  {
    // Get specific user config
    return this.http.get<any>(
      environment.apiAuthEndpoint + environment.apiAuthEndpointUrl.config +'?uuid='+ id ,
      this.options)
      .pipe(map(res=>{
        if (res != null)
        {
          this.notificationService.makeToast("Config Service","Configuration exists for user",NbToastStatus.SUCCESS,true)

          return res
        }
        else
        {
          this.notificationService.makeToast("Config Service","No configuration for user found",NbToastStatus.WARNING,true)
        }
        return res
      }))
  }

  getAllConfigs()
  {
    return this.http.get<any>(
      environment.apiAuthEndpoint + environment.apiAuthEndpointUrl.config ,
      this.options)
      .pipe(map(res=>{
        return res
      }))
  }

  updateConfig(result:any)
  {
    // Update user config
    return this.http.patch(
      environment.apiAuthEndpoint + environment.apiAuthEndpointUrl.config +'/'+result.id,
      {
        "ph":result.ph,
        "ao":result.ao,
        "lch":result.lch,
        "nc":result.nc,
        "tcu":result.tcu,
        "uuid":result.uuid
      })
      .subscribe(
        data  => {
          console.log("(UtilityService): updateUserConfig response: " +JSON.stringify(data) )
          this.notificationService.makeToast("Config Service","Created configuration for user",NbToastStatus.SUCCESS,true)
          this.router.navigate(['/creditor/dashboard'])
        },
        error  => {
          var err_msg = "Error: [" + error + "] in updating configuration for user"
          this.notificationService.makeToast("Config Service",err_msg,NbToastStatus.SUCCESS,true)
    });
  }

  getAllCreditors()
  {
    return this.http.get<any>(
      environment.apiAuthEndpoint + environment.apiAuthEndpointUrl.creditor ,
      this.options)
      .pipe(map(res=>{
        return res
      }))
  }

  getAllAccountType()
  {
    return this.http.get<any>(
      environment.apiAuthEndpoint + environment.apiAuthEndpointUrl.accountType ,
      this.options)
      .pipe(map(res=>{
        return res
      }))
  }

}
