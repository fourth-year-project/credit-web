import { Injectable } from '@angular/core';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { ToasterConfig } from 'angular2-toaster';

@Injectable()
export class NotificationService {

  constructor(private toastrService: NbToastrService) {}

  config: ToasterConfig;

  index = 1;
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = true;
  status: NbToastStatus = NbToastStatus.SUCCESS;


  // types: NbToastStatus[] = [
  //   NbToastStatus.DEFAULT,
  //   NbToastStatus.DANGER,
  //   NbToastStatus.INFO,
  //   NbToastStatus.PRIMARY,
  //   NbToastStatus.SUCCESS,
  //   NbToastStatus.WARNING,
  // ];
  // positions: string[] = [
  //   NbGlobalPhysicalPosition.TOP_RIGHT,
  //   NbGlobalPhysicalPosition.TOP_LEFT,
  //   NbGlobalPhysicalPosition.BOTTOM_LEFT,
  //   NbGlobalPhysicalPosition.BOTTOM_RIGHT,
  //   NbGlobalLogicalPosition.TOP_END,
  //   NbGlobalLogicalPosition.TOP_START,
  //   NbGlobalLogicalPosition.BOTTOM_END,
  //   NbGlobalLogicalPosition.BOTTOM_START,
  // ];


  makeToast(title:string,content:string,type:NbToastStatus,icon:boolean,position?:NbGlobalPhysicalPosition) {
    this.showToast( title, content,type, icon,position);
  }

  private showToast(title: string, body: string,type: NbToastStatus, icon: boolean,position?:NbGlobalPhysicalPosition) {
    let position_d
    if(position != null )
    { position_d =position }
    else
    {
      position_d = NbGlobalPhysicalPosition.TOP_RIGHT
    }
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: icon,
      position: position_d,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }
}
