import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';

@Injectable()

export class HelperService {

    constructor(private datePipe: DatePipe)
    {}

    convertDateForDisplay(date:Date)
    {
        return this.datePipe.transform(date, 'yyyy-MM-dd');
    }
    convertDateToString(date:Date)
    {
        return date.toISOString()
        // return this.datePipe.transform(date.toISOString());
    }

    convertStringToDate(date:string)
    {
        return new Date(date)
    }

    getCurrentDateDiff(date:string)
    {
       var today = new Date
       var timeDiff = Math.abs( today.getTime() -   this.convertStringToDate(date).getTime() );

       return Math.ceil(timeDiff / (1000 * 3600 * 24));
    }

    getDateDiff(date1:string,date2:string)
    {
       var timeDiff = Math.abs( this.convertStringToDate(date1).getTime() -   this.convertStringToDate(date2).getTime() );

       return Math.ceil(timeDiff / (1000 * 3600 * 24));
    }



    convertMS( milliseconds ) {
      var day, hour, minute, seconds;
      seconds = Math.floor(milliseconds / 1000);
      minute = Math.floor(seconds / 60);
      seconds = seconds % 60;
      hour = Math.floor(minute / 60);
      minute = minute % 60;
      day = Math.floor(hour / 24);
      hour = hour % 24;
      return {
          day: day,
          hour: hour,
          minute: minute,
          seconds: seconds
      };
    }
}
