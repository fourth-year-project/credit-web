import { Injectable } from '@angular/core';
import { ITransactionHistory } from '../data/transactionHistory';
import { IOrganisation } from '../data/organisation';
import { HelperService } from './helper.service';
import { UtilityService } from './utility.service';
import { ILoan } from '../data/loan';
import { IAccount } from '../data/account';

@Injectable()
export class CreditScoreService{

    defaulWeights ={
      ph:35,
      ao:30,
      lch:15,
      nc:10,
      tcu:10
    }

    constructor(
      private helper:HelperService,
      private utilService:UtilityService
      )
    {
    }

    compute(transHistData:ITransactionHistory[],organisation:IOrganisation, loansData:ILoan[],accountsData:IAccount[],weights?:any)
    {

      if (weights != null)
      {
        this.defaulWeights.ph= weights.ph
        this.defaulWeights.ao= weights.ao
        this.defaulWeights.lch= weights.lch
        this.defaulWeights.nc= weights.nc
        this.defaulWeights.tcu= weights.tcu
      }

      let transHist:ITransactionHistory[] = transHistData
      let loans:ILoan[] = loansData
      let accounts:IAccount[] = accountsData
      let org:IOrganisation = organisation

      let currentAccountList:any[] =[]

      let recentSales:number = 0
      let allSales:number = 0

      let recurrentExp:number = 0
      let loanRepay:number = 0
      let oneOffCosts:number = 0
      let savingsTotal:number = 0
      let recordFreqUpdate:number = 0

      let paymentScore:number =0
      let debt:number =0
      let creditLength = 0
      let pressure =0
      let accountVariety = 0

      Object.keys(loans).forEach(loan_item=>{

          let repayAmount:number= 0
          Object.keys(transHist).forEach(trans_item=>{

            if( this.helper.getDateDiff (loans[loan_item]._issueDate,transHist[trans_item]._transactionPeriodEnd ) > 0)
            {
              repayAmount= repayAmount + transHist[trans_item]._loanRepayment
            }

            // Saving Total
            savingsTotal = savingsTotal + transHist[trans_item]. _savingAmount
          })

          // Get debt
          debt = debt + loans[loan_item]._amount

          // Credit length
          if (this.helper.getCurrentDateDiff(loans[loan_item]._issueDate) > creditLength )
          {
            creditLength = this.helper.getCurrentDateDiff(loans[loan_item]._issueDate)
          }

          // Get Payment History Score
          let paymentRatio:number = (repayAmount/loans[loan_item]._amount)

          if(paymentRatio > 99)
          {
              paymentScore = paymentScore + 3
          }
          else if (paymentRatio > 80 )
          {
            paymentScore = paymentScore + 2
          }
          else
          {
            paymentScore = paymentScore + 1
          }
      })

      // Financial Pressure
      Object.keys(accounts).forEach(acc_item=>{
        Object.keys(loans).forEach(loan_item=>{

          var dateDiff = this.helper.getDateDiff ( accounts[acc_item].__created, loans[loan_item].__issueDate)
          if( 0 <  dateDiff &&  dateDiff < 90)
          {
            pressure = pressure + 2
          }

          pressure = pressure + 1

        })

        if( currentAccountList.indexOf(accounts[acc_item]._accountName) == -1 )
        {
          currentAccountList.push(accounts[acc_item]._accountName)
          accountVariety = accountVariety + 1
        }
      })


      return {
              paymentHistory:this.calculatePaymentHistoryScore(paymentScore,loans.length) ,
              amountOwed:this.calculateAmountOwedScore(debt,savingsTotal),
              creditHistory:this.calculateLengthOfCreditHistoryScore(creditLength),
              newCredit: this.calculateNewCreditScore(pressure),
              creditType:this.calculateTypeOfCreditUsedScore(accountVariety),
            }
    }

    calculatePaymentHistoryScore(score:number, count:number)
    {
      let explaination:string=""

      if(  score > (2.5 * count) )
      {
        explaination = "Excellent"
      }
      else  if (  score > (1.5 * count))
      {
        explaination = "Average (missed some deadlines)"
      }
      else
      {
        explaination = "Poor (missed plenty deadlines)"
      }

      return {score: Math.floor( score/ (3*count) * this.defaulWeights.ph ) , explain:explaination}
    }

    calculateAmountOwedScore( debt:number, savingAmount:number)
    {
      let percentageOf = Math.floor( (debt/savingAmount)*100)
      let explaination:string=""


      if( percentageOf>99)
      {
        percentageOf=100
        explaination = "Bankrupt"

      }
      else if(percentageOf > 70 )
      {
        explaination = "Drowning in debt"
      }
      else if (percentageOf > 40)
      {
        explaination = " Managable debt"
      }
      else
      {
        explaination = " Healthy debt"
      }

      return {score: Math.floor( ( 100 - percentageOf )/100 * this.defaulWeights.ao ) , explain:explaination}

    }

    calculateLengthOfCreditHistoryScore(length:number)
    {
      // loans
      let explaination:string=""

      length = length/365

      if(length > 15)
      {
        explaination= "Veteran (Excellent) "
      }
      else if(length > 11)
      {
        explaination="Veteran (Exceptional)"
      }
      else if(length > 7)
      {
        explaination="Medium (Good)"
      }
      else if(length > 3)
      {
        explaination="Medium (Average)"
      }
      else if(length > 1)
      {
        explaination="Medium (Below Average)"
      }
      else{
        explaination="Short (Unsatisfactory)"
      }

      return {score: Math.floor(length)  , explain:explaination}

    }

    calculateNewCreditScore(pressureScale:number)
    {
      let newCreditScore:number= (100 - pressureScale)/100
      let explaination:string=""

      if (pressureScale > 7 )
      {
        explaination = "Intense pressure, financial difficulties"
      }
      else if(pressureScale > 4)
      {
        explaination = "Moderate financial pressure"
      }
      else
      {
        explaination = "Little financial pressure"
      }

      return {score: Math.floor( newCreditScore / this.defaulWeights.nc)  , explain:explaination}

    }

    calculateTypeOfCreditUsedScore(creditAvailable:number)
    {
      //loan and account
      let creditUsedScore:number=0
      let explaination:string=""

      if (creditAvailable > 5 )
      {
        creditUsedScore = 4.5
        explaination = "Excellent mix"
      }
      else if(creditAvailable > 3)
      {
        creditUsedScore = 3
        explaination = "Rich mix"
      }
      else if(creditAvailable > 1)
      {
        creditUsedScore = 1.5
        explaination = "Conservative mix"
      }
      else
      {
        creditUsedScore = 0.5
        explaination = "Poor"
      }

      return {score: Math.floor( creditUsedScore/5* this.defaulWeights.tcu ) , explain:explaination}
    }
}
