import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { ClientsComponent } from './clients.component';
import { ListItemComponent } from './list-item/list-item.component';
import { SharedModule } from '../../pages/shared/shared.module';

@NgModule({
  declarations: [
    ClientsComponent,
    ListItemComponent,
  ],
  imports: [
    ThemeModule,
    SharedModule
  ]
})
export class ClientsModule { }
