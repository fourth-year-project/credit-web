import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { IOrganisation } from '../../../@core/data/organisation';

@Component({
  selector: 'creditor-client-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent {

  @Input() organisation: IOrganisation

  constructor() { }

}
