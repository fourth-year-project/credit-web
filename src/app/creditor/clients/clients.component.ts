import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WatchDogService } from '../../@core/service/watchdog.service';
import { OrganisationService } from '../../@core/service/organisation.service';
import { IOrganisation } from '../../@core/data/organisation';
import { TransactionHistoryService } from '../../@core/service/transactionhistory.service';
import { CreditScoreService } from '../../@core/utils/credit-score.service';
import { ITransactionHistory } from '../../@core/data/transactionHistory';
import { UserService } from '../../@core/service/user.service';
import { IUser } from '../../@core/data/users';
import { ILoan } from '../../@core/data/loan';
import { IAccount } from '../../@core/data/account';

@Component({
  selector: 'creditor-clients',
  templateUrl: './clients.component.html',
})
export class ClientsComponent implements OnInit {

  cardData = {
    items: [],
    placeholders: [],
    loading: false,
    pageToLoadNext: 1,
  };

  pageSize = 1;

  organisationListReady:boolean = false
  watchdogsListReady:boolean = false
  transHistListReady: boolean = false

  userDetails = <IUser>{}
  orgDetails = <IOrganisation>{}
  allUserList:any[] = []

  allLoansList:any[]=[]
  allAccountsList:any[]=[]

  userAccountsList:any[]=[]
  userLoanList:any[] =[]

  organisationList : any[]
  watchdogsList : any[]
  transHistList : any[]
  currentItemsList:IOrganisation[] =[]
  removedWatchdogList:any[] = []

  allUserConfigList:any[] = []

  userConfig:any

  constructor(
    private router:Router,
    private route: ActivatedRoute,
    private watchdogService: WatchDogService,
    private organisationService: OrganisationService,
    private transHistService: TransactionHistoryService,
    private creditService: CreditScoreService,
    private userService: UserService,

  )
  {}

  ngOnInit()
  {

    this.allUserList = this.route.snapshot.data.allUsers
    this.organisationList = this.route.snapshot.data.allOrgs
    this.transHistList = this.route.snapshot.data.transactionHistory
    this.allUserConfigList = this.route.snapshot.data.userConfig

    this.allLoansList = this.route.snapshot.data.allLoans
    this.allAccountsList = this.route.snapshot.data.allAccounts

    // console.log("(ClientComponent) : AccountsList" + JSON.stringify( this.allLoansList))

    // TODO: Check for null error
    Object.keys(this.allUserConfigList).forEach(element => {

        if (this.allUserConfigList[element]._uuid == this.userService.currentUserValue()._uuid)
        {
          this.userConfig = this.allUserConfigList[element]
          // console.log("User Config : " + JSON.stringify(this.userConfig))
        }
    });

    Object.keys(this.allUserList).forEach(element =>
    {
      if (this.allUserList[element]._uuid == this.userService.currentUserValue()._uuid)
      {
        this.userDetails= this.allUserList[element]

      }
    });
      // console.log("User Details : " + JSON.stringify(this.userDetails))


    Object.keys(this.organisationList).forEach(element=>
    {
      if(this.organisationList[element]._id == this.userDetails._organisation )
      {
        this.orgDetails = this.organisationList[element]
      }
    })

    // console.log("Org details" + JSON.stringify(this.orgDetails) )
    // this.loadAllOrganisationList()
    this.organisationListReady = true
    this.loadAllWatchdogList()

  }



  loadAllWatchdogList()
  {
    this.watchdogService.getAllWatchDogs().subscribe(res=>
      {
        this.watchdogService.getWatchdogsList().subscribe(res=>
          {
            this.watchdogsList =res
            this.watchdogsListReady = true
          })
        // console.log("{ClientsComponent} - (getAllWatchdogList):[list] : " +JSON.stringify(this.watchdogsList) )
      })
  }


  back()
  {
    this.router.navigate(['/creditor/clients' ])
  }

  loadNext(cardData) {
    if (cardData.loading) { return; }

    cardData.loading = true;
    cardData.placeholders = new Array(this.pageSize);

    let organisationList : IOrganisation[] = [];
    let transHistList : ITransactionHistory[] = [];
    let userLoanList: ILoan[]=[]
    let userAccountList:IAccount[] =[]

    this.watchdogService.getAllWatchDogs().subscribe(res=>
    {
        Object.keys(this.watchdogsList).forEach(wd_item=>
        {
            Object.keys(this.organisationList).forEach(org_item=>
            {

                if (this.watchdogsList[wd_item]._authorizingOrganization == this.organisationList[org_item]._id)
                {
                      if(this.watchdogsList[wd_item]._active && this.watchdogsList[wd_item]._authorized)
                      {
                          // Approved
                          if(this.currentItemsList.indexOf(this.organisationList[org_item]._id) === -1 )
                          {
                              organisationList.length = 0
                              this.currentItemsList.push(this.organisationList[org_item]._id)
                              console.log("(ClientsComponent) Added approved item :" + JSON.stringify(this.organisationList[org_item]._id))

                              this.organisationList[org_item]._watchdog_authorized =  this.watchdogsList[wd_item]._authorized
                              this.organisationList[org_item]._watchdog_active =  this.watchdogsList[wd_item]._active


                              Object.keys(this.transHistList).forEach(trans_item =>
                                {
                                  if (this.transHistList[trans_item]._organisationID == this.organisationList[org_item]._id)
                                  {
                                    transHistList.push(this.transHistList[trans_item])
                                  }
                                })

                              Object.keys(this.allAccountsList).forEach(item=>{

                                if(this.allAccountsList[item]._holder == this.organisationList[org_item]._id )
                                {
                                  userAccountList.push(this.allAccountsList[item])
                                }

                              })

                              Object.keys(this.allLoansList).forEach(item=>{

                                if (this.allLoansList[item]._recepient == this.organisationList[org_item]._id )
                                {
                                  userLoanList.push(this.allLoansList[item])
                                }
                              })

                              var scoreDataTemp = this.creditService.compute(transHistList,this.organisationList[org_item],userLoanList,userAccountList,this.userConfig)

                              var scoreData ={
                                score: (scoreDataTemp.paymentHistory.score + scoreDataTemp.amountOwed.score + scoreDataTemp.creditHistory.score + scoreDataTemp.newCredit.score + scoreDataTemp.creditType.score),
                                explain:scoreDataTemp
                              }

                              console.log("Main Event "+  JSON.stringify(scoreData) )

                              this.organisationList[org_item]._creditScoreDataset = scoreData
                              organisationList.push(this.organisationList[org_item])

                              cardData.placeholders = [];
                              cardData.items.push(...organisationList);
                              cardData.loading = false;
                              cardData.pageToLoadNext++;
                            }
                      }

                      else if (!this.organisationList[org_item]._active)
                      {
                        // Remove items
                        var index : number =0
                        index = this.currentItemsList.indexOf(this.organisationList[org_item]._id)
                        // Remove and push changes
                        cardData.items.splice(index,1)
                        cardData.loading = false;
                        cardData.pageToLoadNext++;
                      }

                      // console.log("Current Display org card items :" + JSON.stringify(cardData.items) )
                      // console.log("Current Approved [list] :" + JSON.stringify(this.currentItemsList) )
                }
            })
          })
      })
  }
}
