import { OnInit, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../@core/service/user.service';
import { environment } from '../../../environments/environment';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { UtilityService } from '../../@core/utils/utility.service';
import { NotificationService } from '../../@core/utils/notification.service';
import { distinctUntilChanged, pairwise, startWith } from 'rxjs/operators';

@Component({
  selector: 'creditor-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {

  allUserList:any[] = []
  allUserConfigList:any[] = []

  userData:any=[]

  total:number=0
  ph:number = 0
  ao:number = 0
  lch:number = 0
  nc:number = 0
  tcu:number = 0
  totalAcceptable:boolean = false

  settingForm: FormGroup

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private userService:UserService,
    private utilService:UtilityService,
    private router:Router,

  )
  {}

  ngOnInit() {

    this.createForm()

    this.allUserList = this.route.snapshot.data.userData
    this.allUserConfigList = this.route.snapshot.data.userConfig

    var userValue = this.userService.currentUserValue()

    console.log("(SettingComponent) userValue: " + JSON.stringify(userValue))

      Object.keys(this.allUserList).forEach(item=>{

        if( userValue._uuid  == this.allUserList[item]._uuid  )
        {
          this.userData = this.allUserList[item]
          console.log("(SettingComponent) userData: " + JSON.stringify(this.userData))
        }
      })
  }

  back()
  {
    this.router.navigate([ environment.appRoutes.creditee + environment.crediteeRoutes.loan])
  }

  createForm()
  {
    this.settingForm = this.fb.group({
        'ph': new FormControl('',
        [
          Validators.required,
          Validators.min(0)
        ]),
        'ao': new FormControl('',
        [
          Validators.required,
          Validators.max(98),
          Validators.min(0)
        ]),
        'lch': new FormControl('',
        [
          Validators.required,
          Validators.max(98),
          Validators.min(0)
        ]),
        'nc': new FormControl('',
        [
          Validators.required,
          Validators.max(98),
          Validators.min(0),
          Validators.pattern("/^-?[0-9]+$/")
        ]),
        'tcu': new FormControl('',
        [
          Validators.required,
          Validators.max(98),
          Validators.min(0)
        ]),
      })

      this.settingForm.get('ph').valueChanges.pipe(startWith(null),pairwise()).subscribe(
        ([prev, next]: [any, any]) => {

        if ( prev == null  )
        {
          prev =0
        }
        else if(next == null || next < 0 )
        {
          next = 0
        }
        else
        {}

        this.ph = next
        var new_total = this.total+next-prev
        if ( new_total > 100 )
        {
          this.settingForm.get('ph').setErrors({inaccurate:true})
          this.total = new_total
        }
        else
        {
          this.settingForm.get('ph').setErrors(null)

          console.log(" (PH) Old value : " + prev  +  "  New value : "+ next  + " TOTAL : " +new_total)
          this.total = new_total
        }
        this.checkTotal(this.total)


        });

      this.settingForm.get('ao').valueChanges.pipe(startWith(null),pairwise()).subscribe(
        ([prev, next]: [any, any]) => {

        if ( prev == null  )
        {
          prev =0
        }
        else if(next == null || next < 0 )
        {
          next = 0
        }
        else
        {}

        this.ao = next
        var new_total = this.total+next-prev
        if ( new_total > 100 )
        {
          this.settingForm.get('ao').setErrors({inaccurate:true})
          this.total = new_total
        }
        else
        {
          this.settingForm.get('ao').setErrors(null)

          console.log(" (AO) Old value : " + prev  +  "  New value : "+ next  + " TOTAL : " +new_total)
          this.total = new_total
        }
        this.checkTotal(this.total)


        });

        this.settingForm.get('lch').valueChanges.pipe(startWith(null),pairwise()).subscribe(
          ([prev, next]: [any, any]) => {

          if ( prev == null  )
          {
            prev =0
          }
          else if(next == null || next < 0 )
          {
            next = 0
          }
          else
          {}

          this.lch = next
          var new_total = this.total+next-prev
          if ( new_total > 100 )
          {
            this.settingForm.get('lch').setErrors({inaccurate:true})
            this.total = new_total
          }
          else
          {
            this.settingForm.get('lch').setErrors(null)

            console.log(" (lch) Old value : " + prev  +  "  New value : "+ next  + " TOTAL : " +new_total)
            this.total = new_total
          }
          this.checkTotal(this.total)


          });
          this.settingForm.get('nc').valueChanges.pipe(startWith(null),pairwise()).subscribe(([prev, next]: [any, any]) => {

              if ( prev == null  )
              {
                prev =0
              }
              else if(next == null || next < 0 )
              {
                next = 0
              }
              else
              {}

              this.nc = next
              var new_total = (this.total+ next) - prev
              if ( new_total > 100 )
              {
                this.total = new_total
              }
              else
              {

                if( next == 0)
                {
                  this.settingForm.get('nc').setErrors({required:true,inaccurate:false})
                }
                this.settingForm.get('nc').setErrors(null)

                console.log(" (nc) Old value : " + prev  +  "  New value : "+ next  + " TOTAL : " + new_total)
                this.total = new_total
              }
              this.checkTotal(this.total)


          });
            this.settingForm.get('tcu').valueChanges.pipe(startWith(null),pairwise()).subscribe(
              ([prev, next]: [any, any]) => {

              if ( prev == null  )
              {
                prev =0
              }
              else if(next == null || next < 0 )
              {
                next = 0

              }
              else
              {}

              this.tcu = next
              var new_total = this.total+next-prev
              if ( new_total > 100 )
              {
                this.settingForm.get('tcu').setErrors({inaccurate:true})
                this.total = new_total
              }
              else
              {
                this.settingForm.get('tcu').setErrors(null)

                console.log(" (tcu) Old value : " + prev  +  "  New value : "+ next  + " TOTAL : " +new_total)
                this.total = new_total
              }
              this.checkTotal(this.total)

            });
  }

  checkTotal(num:number)
  {
    if (num==100)
    {
      this.totalAcceptable=true
    }

  }

  onSubmit()
  {
    const result: any = Object.assign({}, this.settingForm.value);

    result.uuid = this.userData._uuid
    console.log("(SettingComponent) Post: " + JSON.stringify(result))

    if (this.userData == null)
    {
      this.utilService.createUserConfig(result)
    }
    else
    {
      this.utilService.updateConfig(result)
    }
  }
}
