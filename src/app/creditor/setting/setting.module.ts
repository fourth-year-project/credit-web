import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeModule } from '../../@theme/theme.module';
import { NbWindowModule } from '@nebular/theme';
import { SettingComponent } from './setting.component';

const COMPONENTS = [
  SettingComponent,
];
const ENTRY_COMPONENTS = [
];

const MODULES = [
  ThemeModule,
  NbWindowModule.forChild(),
];

const SERVICES = [
];


@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,

  ],
  providers: [
    ...SERVICES,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})
export class SettingModule { }
