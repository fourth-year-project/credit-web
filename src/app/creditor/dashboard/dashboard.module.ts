import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { SharedModule } from '../../pages/shared/shared.module';
import { CreditorRoutingModule } from '../creditor-routing.module';

@NgModule({
  declarations: [
    DashboardComponent,
  ],
  imports: [
    ThemeModule,
    SharedModule,
  ]
})
export class DashboardModule { }
