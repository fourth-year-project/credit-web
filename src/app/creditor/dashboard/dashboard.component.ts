import { Component, OnInit } from '@angular/core';
import { OrganisationService } from '../../@core/service/organisation.service';
import { TransactionHistoryService } from '../../@core/service/transactionhistory.service';
import { CreditScoreService } from '../../@core/utils/credit-score.service';
import { IOrganisation } from '../../@core/data/organisation';
import { WatchDogService } from '../../@core/service/watchdog.service';
import { map } from 'rxjs/operators';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition } from '@nebular/theme';
import { NotificationService } from '../../@core/utils/notification.service';
import { ActivatedRoute } from '@angular/router';
import { IUser } from '../../@core/data/users';
import { UserService } from '../../@core/service/user.service';

@Component({
  selector: 'creditor-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit
{
  userDetails = <IUser>{}
  orgDetails :IOrganisation
  allTransHistList : any[] = []
  orgTransHistList : any[] =[]
  allWatchdogList : any[] = []
  allOrgsList : any[] = []
  allUserList:any[] = []

  allLoansList:any[]=[]
  allAccountsList:any[]=[]

  userAccountsList:any[]=[]
  userLoanList:any[] =[]

  ChartDataReady:boolean = false

  results:any=[]

  currentOrgTransHistList:any[]=[]
  organisationList : any[] =[]

  allUserConfigList:any[] = []

  userConfig:any

  private creditStatistics:any

  constructor(
    private route: ActivatedRoute,
    private crediScoreService: CreditScoreService,
    private orgService: OrganisationService,
    private userService: UserService,
  )
  {}

  ngOnInit()
  {
    this.orgDetails = this.route.snapshot.data.organisationData
    this.allTransHistList = this.route.snapshot.data.allTransHist
    this.organisationList = this.route.snapshot.data.allOrgs
    // this.allOrgsList= this.route.snapshot.data.allOrgs
    this.allUserList = this.route.snapshot.data.allUsers

    this.allWatchdogList = this.route.snapshot.data.allWatchdogs
    this.allUserConfigList = this.route.snapshot.data.userConfig

    this.allLoansList = this.route.snapshot.data.allLoans
    this.allAccountsList = this.route.snapshot.data.allAccounts

    console.log( "Test all"+JSON.stringify(this.allWatchdogList))


    // TODO: Check for null error
    Object.keys(this.allUserConfigList).forEach(element => {

        if (this.allUserConfigList[element]._uuid == this.userService.currentUserValue()._uuid)
        {
          this.userConfig = this.allUserConfigList[element]
          console.log("User Config : " + JSON.stringify(this.userConfig))
        }
    });

    Object.keys(this.allUserList).forEach(element => {

        if (this.allUserList[element]._uuid == this.userService.currentUserValue()._uuid)
        {
          this.userDetails= this.allUserList[element]
          console.log("User details : " + JSON.stringify(this.userDetails))
        }
    });

    Object.keys(this.organisationList).forEach(element=>
      {
        if(this.organisationList[element]._id == this.userDetails._organisation )
        {
          this.orgDetails = this.organisationList[element]
          console.log("Org details : " + JSON.stringify(this.organisationList[element]) )

        }

      })

    console.log ("(DashboardComponent) : Organisation details " + JSON.stringify(this.orgDetails) )


    this.userAccountsList.length =0
    this.userLoanList.length =0
    // TODO:Test null handlig results
    Object.keys(this.organisationList).forEach(org_item=>{
        // console.log("(DashboardCOmponent) : lvl0 " + JSON.stringify(this.allWatchdogList) )

        Object.keys(this.allWatchdogList).forEach(wd_item =>{
            if(this.organisationList[org_item]._id == this.allWatchdogList[wd_item]._authorizingOrganization)
            {
              console.log("(DashboardCOmponent) : lvl " + JSON.stringify(this.allWatchdogList[wd_item]._authorizedOrganization) + " => id: "+ this.orgDetails._id)

                if (this.allWatchdogList[wd_item]._authorizedOrganization == this.orgDetails._id)
                {
                  // console.log("(DashboardCOmponent) : lvl1 ")
                  if (this.allWatchdogList[wd_item]._active && this.allWatchdogList[wd_item]._authorized)
                  {
                    // console.log("(DashboardCOmponent) : lvl2 ")

                      Object.keys(this.allTransHistList).forEach(item =>{

                        // console.log("(DashboardCOmponent) : lvl3 ")

                        if (this.allTransHistList[item]._organisationID == this.allWatchdogList[wd_item]._authorizingOrganization)
                        {
                          // Check if already in list
                          if(this.currentOrgTransHistList.indexOf(this.allTransHistList[item]._id) === -1 )
                          {
                            this.currentOrgTransHistList.push(this.allTransHistList[item]._id)
                            this.orgTransHistList.push(this.allTransHistList[item])
                          }
                        }
                      })

                      Object.keys(this.allAccountsList).forEach(item=>{

                        if(this.allAccountsList[item]._holder == this.organisationList[org_item]._id )
                        {
                          this.userAccountsList.push(this.allAccountsList[item])
                        }

                      })


                      Object.keys(this.allLoansList).forEach(item=>{

                        if (this.allLoansList[item]._recepient == this.organisationList[org_item]._id )
                        {
                          this.userLoanList.push(this.allLoansList[item])
                        }
                      })

                  }
                }
            }
        })


        if (this.orgTransHistList.length > 0 )
        {
            let creditscoreComputeResults = this.crediScoreService.compute(this.orgTransHistList,this.organisationList[org_item],this.userLoanList, this.userAccountsList,this.userConfig)

            let score = (
                      creditscoreComputeResults.paymentHistory.score +
                      creditscoreComputeResults.amountOwed.score +
                      creditscoreComputeResults.creditHistory.score +
                      creditscoreComputeResults.newCredit.score +
                      creditscoreComputeResults.creditType.score)

            console.log("Dash scores: " + JSON.stringify(this.results) )

            this.results.push(score)


            this.creditStatistics  = {
                                        high : Math.max.apply(Math, this.results ),
                                        low : Math.min.apply(Math, this.results ),
                                        mean : (this.results.reduce(function(a, b) { return a + b; }) /this.results.length) ,
                                        count :this.results.length
                                      }
            this.ChartDataReady = true
        }

        else
        {
          this.creditStatistics  = {
            high : 0,
            low :  0 ,
            mean : 0,
            count : 0
          }
          this.ChartDataReady = false

        }
    })

    console.log("Client List:" + JSON.stringify(this.currentOrgTransHistList))

    console.log("Client Accounts List:" + JSON.stringify(this.userAccountsList))

    console.log("Client Loans List:" + JSON.stringify(this.userLoanList))


  }

}
