import { NgModule } from '@angular/core';

import { CreditorRoutingModule } from './creditor-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { CreditorComponent } from './creditor.component';
import { ClientHistoryModule } from './clients-history/client-history.module';
import { ClientsModule } from './clients/clients.module';
import { RequestsModule } from './requests/requests.module';
import { SettingModule } from './setting/setting.module';

const CREDITOR_COMPONENTS = [
  CreditorComponent,
];

@NgModule({
  imports: [
    CreditorRoutingModule,
    ThemeModule,

    ClientsModule,
    ClientHistoryModule,
    DashboardModule,
    RequestsModule,
    SettingModule
  ],
  declarations: [
    ...CREDITOR_COMPONENTS,
  ],
})
export class CreditorModule { }
