import { RequestsComponent } from './requests.component';
import { ThemeModule } from '../../@theme/theme.module';
import { NgModule } from '@angular/core';
import { ListItemComponent } from './list-item/list-item.component';
import { NbWindowModule } from '@nebular/theme';

  const COMPONENTS = [
    RequestsComponent,
    ListItemComponent,
  ];
  const ENTRY_COMPONENTS = [
  ];
  
  const MODULES = [
    ThemeModule,
    NbWindowModule.forChild(),
  ];
  
  const SERVICES = [
  ];
  

  @NgModule({
    imports: [
      ...MODULES,
    ],
    declarations: [
      ...COMPONENTS,
  
    ],
    providers: [
      ...SERVICES,
    ],
    entryComponents: [
      ...ENTRY_COMPONENTS,
    ],
  })
  export class RequestsModule { }
  