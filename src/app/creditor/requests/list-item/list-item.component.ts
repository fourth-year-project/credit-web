import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IOrganisation } from '../../../@core/data/organisation';
import { Router } from '@angular/router';

@Component({
  selector: 'creditor-request-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent {

  @Input() organisations: IOrganisation
  @Output() authorizeWatchdogEvent = new EventEmitter<string>();


  constructor(private router:Router) { }

  viewOrganisationPage(message:string)
  {
    console.log("Creditor Request (listItemCOmponent): loading org page")
    this.router.navigate(['/creditor/client_details/' + message.split('/').join('%2F') ])
  }

  authorizeWatchdog(message:string)
  {
    this.authorizeWatchdogEvent.emit(message)
  }


}
