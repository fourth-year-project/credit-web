import { Component, OnInit } from '@angular/core';
import { WatchDogService } from '../../@core/service/watchdog.service';
import { OrganisationService } from '../../@core/service/organisation.service';
import { IOrganisation } from '../../@core/data/organisation';

@Component({
  selector: 'creditor-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.scss']
})
export class RequestsComponent implements OnInit {

  pendingCardData = {
    items: [],
    placeholders: [],
    loading: false,
    pageToLoadNext: 1,
  };

  approvedCardData = {
    items: [],
    placeholders: [],
    loading: false,
    pageToLoadNext: 1,
  };

  pageSize = 1;

  organisationList : any[]
  watchdogsList : any[]
  currentApprovedItemsList:IOrganisation[] =[]
  currentPendingItemsList:IOrganisation[] =[]


  constructor(
    private watchdogService: WatchDogService,
    private organisationService: OrganisationService,
  ) { }

  ngOnInit() {
    this.organisationService.getAllOrganisations().subscribe(res =>{
      this.organisationService.getOrganisationsList().subscribe(res=>
        {
          this.organisationList = res

        })
        console.log("{RequestsComponent} - (getAllOrganisations):[list] : " +JSON.stringify(this.organisationList) )
    })


    this.watchdogService.getAllWatchDogs().subscribe(res=>
      {
        // console.log("{WatchdogComponent} - (getAllWatchdogs):[list] : " +JSON.stringify(res) )
        this.watchdogService.getWatchdogsList().subscribe(res=>this.watchdogsList =res)
        console.log("{WatchdogComponent} - (getAllWatchdogList):[list] : " +JSON.stringify(this.watchdogsList) )
      })
  }

  receiveAuthorizeOrganisation($event)
  {
    console.log("(RequestsComponent) received approval: "+ $event)

    this.watchdogService.cancelWatchdogRights($event)
  }

  // TODO: fix branching for pending and approved data passthrough
  loadNext(cardData) {
    if (cardData.loading) { return; }

    cardData.loading = true;
    cardData.placeholders = new Array(this.pageSize);

    let organisationList : IOrganisation[] = [];

    this.watchdogService.getAllWatchDogs().subscribe(res=>
    {
      this.watchdogsList = res

      Object.keys(this.watchdogsList).forEach(wd_item=>
      {
        Object.keys(this.organisationList).forEach(org_item=>
        {
            if (this.watchdogsList[wd_item]._authorizingOrganization == this.organisationList[org_item]._id)
            {
                  if(this.watchdogsList[wd_item]._active && this.watchdogsList[wd_item]._authorized)
                  {
                      // Approved
                      if(this.currentApprovedItemsList.indexOf(this.organisationList[org_item]._id) === -1 )
                      {
                          organisationList.length = 0
                          this.currentApprovedItemsList.push(this.organisationList[org_item]._id)
                          console.log("(RequestsComponent) Added approved item :" + JSON.stringify(this.organisationList[org_item]._id))

                          this.organisationList[org_item]._watchdog_authorized =  this.watchdogsList[wd_item]._authorized
                          this.organisationList[org_item]._watchdog_active =  this.watchdogsList[wd_item]._active

                          organisationList.push(this.organisationList[org_item])

                          this.approvedCardData.placeholders = [];
                          this.approvedCardData.items.push(...organisationList);
                          this.approvedCardData.loading = false;
                          this.approvedCardData.pageToLoadNext++;
                        }
                        console.log("Current Approved [list] :" + JSON.stringify(this.currentApprovedItemsList) )

                  }
                  else if (this.watchdogsList[wd_item]._active && !this.watchdogsList[wd_item]._authorized)
                  {
                      // Pending
                      if(this.currentPendingItemsList.indexOf(this.organisationList[org_item]._id) === -1 )
                      {
                          organisationList.length = 0
                          this.currentPendingItemsList.push(this.organisationList[org_item]._id)
                          console.log("(RequestsComponent) Added pending item :" + JSON.stringify(this.organisationList[org_item]._id))

                          this.organisationList[org_item]._watchdog_authorized =  this.watchdogsList[wd_item]._authorized
                          this.organisationList[org_item]._watchdog_active =  this.watchdogsList[wd_item]._active

                          organisationList.push(this.organisationList[org_item])

                          this.pendingCardData.placeholders = [];
                          this.pendingCardData.items.push(...organisationList);
                          this.pendingCardData.loading = false;
                          this.pendingCardData.pageToLoadNext++;
                      }
                      console.log("Current Pending  [list] :" + JSON.stringify(this.currentApprovedItemsList) )

                  }
                  // else if (!this.watchdogsList[wd_item]._active)
                  // {
                  //   // Remove items
                  //   var index : number =0
                  //   index = this.currentPendingItemsList.indexOf(this.organisationList[org_item]._id)
                  //   if( index  == -1 )
                  //   {
                  //     index=0
                  //     index = this.currentApprovedItemsList.indexOf(this.organisationList[org_item]._id)
                  //   }
                  //   // Remove and push changes
                  //   cardData.items.splice(index,1)
                  //   cardData.loading = false;
                  //   cardData.pageToLoadNext++;
                  // }
                  else
                  {}
                  // console.log("Current Display org card items :" + JSON.stringify(cardData.items) )
            }
          })
        })
    })
  }

}
