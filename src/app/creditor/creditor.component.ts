import { Component } from '@angular/core';
import { CREDITOR_MENU_ITEMS } from './creditor-menu';


@Component({
  selector: 'ngx-creditee',
  styleUrls: ['creditor.component.scss'],
  template: `
    <ngx-sample-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-sample-layout>
  `,
})
export class CreditorComponent {

  menu = CREDITOR_MENU_ITEMS;
}
