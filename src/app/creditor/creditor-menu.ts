import { NbMenuItem } from '@nebular/theme';

export const CREDITOR_MENU_ITEMS: NbMenuItem[] = [

  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: 'dashboard',
    home: true
  },
  {
    title: 'Clients',
    icon: 'nb-person',
    link: 'clients'
  },
  {
    title: 'Requests',
    icon: 'nb-compose',
    link: 'requests',
  },
  {
    title: 'Setting',
    icon: 'nb-compose',
    link: 'setting',
  },


];
