import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { ClientHistoryComponent } from './client-history.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../pages/shared/shared.module';

@NgModule({
  declarations: [
    ClientHistoryComponent,
  ],
  imports: [
    SharedModule,
    ThemeModule,
    NgxEchartsModule,
    NgxChartsModule
  ]
})
export class ClientHistoryModule { }
