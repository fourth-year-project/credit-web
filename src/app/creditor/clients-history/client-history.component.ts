import { Component, AfterViewInit, OnDestroy, OnInit } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { TestBed } from '@angular/core/testing';
import { TransactionHistoryService } from '../../@core/service/transactionhistory.service';
import { OrganisationService } from '../../@core/service/organisation.service';
import { IOrganisation } from '../../@core/data/organisation';
import { Observable } from 'rxjs';
import { CreditScoreService } from '../../@core/utils/credit-score.service';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../@core/service/user.service';
import { IUser } from '../../@core/data/users';

@Component({
  selector: 'creditor-client-history',
  templateUrl: './client-history.component.html',
})
export class ClientHistoryComponent implements OnInit, AfterViewInit, OnDestroy
{
  private creditscoreDataset= {}

  chartData:any={}
  AllTransHistList : any[] =[]
  orgTransHistList : any[] =[]
  allUsersList : any[] =[]
  allLoansList:any[]=[]
  allAccountsList:any[]=[]

  userAccountsList:any[]=[]
  userLoanList:any[] =[]


  managerDetails:IUser
  organisationDetails:IOrganisation;

  AllTransHistListReady: boolean = false

  multi:number[][]
  options: any = {};
  themeSubscription: any;

  pageLoaded=false
  legendList:any=[]
  private sub:any
  private page_id:any


  allUserConfigList:any[] = []

  userConfig:any

  constructor(private theme: NbThemeService,
    private route: ActivatedRoute,
    private transHistService: TransactionHistoryService,
    private organisationService: OrganisationService,
    private userService:UserService,
    private crediScoreService:CreditScoreService,
    ) {
    this.legendList =['Loan Repayment', 'Investments', 'Savings', 'Recurrent Expenditure']
  }

  ngOnInit()
  {

      // console.log("Fecthed data " + JSON.stringify(this.route.snapshot.data) )

      this.AllTransHistList = this.route.snapshot.data.allTransHist

      this.organisationDetails = this.route.snapshot.data.organisationData

      this.allUsersList = this.route.snapshot.data.allUsers

      this.allUserConfigList = this.route.snapshot.data.userConfig

      this.allLoansList = this.route.snapshot.data.allLoans

      this.allAccountsList = this.route.snapshot.data.allAccounts

      console.log("Fecthed data " + JSON.stringify(this.route.snapshot.data.allTransHist) )


      // TODO: Check for null error
      Object.keys(this.allUserConfigList).forEach(element => {

          if (this.allUserConfigList[element]._uuid == this.userService.currentUserValue()._uuid)
          {
            this.userConfig = this.allUserConfigList[element]
            console.log("User Config : " + JSON.stringify(this.userConfig))
          }
      });

      console.log("Fecthed data " + JSON.stringify(this.allUsersList) )


      for (let item in  this.allUsersList)
      {
        if (this.organisationDetails._id == this.allUsersList[item]._organisation )
        {
          this.managerDetails = this.allUsersList[item]
        }
      }

      Object.keys(this.allAccountsList).forEach(item=>{

        if(this.allAccountsList[item]._holder == this.organisationDetails._id )
        {
          this.userAccountsList.push(this.allAccountsList[item])
        }

      })

      Object.keys(this.allLoansList).forEach(item=>{

        if (this.allLoansList[item]._recepient ==this.organisationDetails._id )
        {
          this.userLoanList.push(this.allLoansList[item])
        }
      })

      this.loadAllTransactionHistoryList()
  }

  loadAllTransactionHistoryList()
  {
      // console.log("TransHist List"+ JSON.stringify(this.AllTransHistList) )
      this.AllTransHistList.sort(function(a, b) {
          // Sort ascending
          var dateA = new Date(a._transactionPeriodStart), dateB = new Date(b._transactionPeriodStart);
          return dateA.getTime() - dateB.getTime();
      });

      this.AllTransHistListReady =true

      // console.log("(Client-history-component) finished loading allTransList ")
      this.loadOtherData()
  }

  loadOtherData()
  {
    // console.log("(Client-history-component) loadOtherData called "+this.AllTransHistListReady)
    // console.log("Org detail :"+ JSON.stringify(this.organisationDetails) )

    let transItemsStartDate:any=[]
    let transItemsLoanRepay:any=[]
    let transItemsOneOffCosts:any=[]
    let transItemsRecurrentExpenditure:any=[]
    let transItemsSavingAmount:any=[]

    Object.keys(this.AllTransHistList).forEach(item =>{
      if (this.AllTransHistList[item]._organisationID == this.organisationDetails._id)
      {
        this.orgTransHistList.push(this.AllTransHistList[item])

        transItemsStartDate.push(this.AllTransHistList[item]._transactionPeriodStart)
        transItemsLoanRepay.push(this.AllTransHistList[item]._loanRepayment)
        transItemsOneOffCosts.push(this.AllTransHistList[item]._oneOffCost)
        transItemsRecurrentExpenditure.push(this.AllTransHistList[item]._recurrentExpenditure)
        transItemsSavingAmount.push(this.AllTransHistList[item]._savingAmount)
      }
    })
    let creditscoreComputeResults = this.crediScoreService.compute(this.orgTransHistList,this.organisationDetails,this.userLoanList, this.userAccountsList,this.userConfig)

    this.creditscoreDataset = {
      score: (creditscoreComputeResults.paymentHistory.score +
              creditscoreComputeResults.amountOwed.score +
              creditscoreComputeResults.creditHistory.score +
              creditscoreComputeResults.newCredit.score +
              creditscoreComputeResults.creditType.score),
      explain:creditscoreComputeResults
    }

    this.chartData = {
      xAxis :transItemsStartDate ,
      legend:this.legendList,
      series:
      {
          loanRepayment: transItemsLoanRepay,
          oneOff: transItemsOneOffCosts,
          recurrentExpenditure: transItemsRecurrentExpenditure,
          savings:transItemsSavingAmount
      }
    }
    this.pageLoaded =true

  }


  ngAfterViewInit() {

    if (this.pageLoaded)
    {
      this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

        const colors: any = config.variables;
        const echarts: any = config.variables.echarts;

        this.options = {
          backgroundColor: echarts.bg,
          color: [colors.warningLight, colors.infoLight, colors.dangerLight, colors.successLight, colors.primaryLight],
          tooltip: {
            trigger: 'axis',
            axisPointer: {
              type: 'cross',
              label: {
                backgroundColor: echarts.tooltipBackgroundColor,
              },
            },
          },
          legend: {
            data: this.chartData.legend,
            textStyle: {
              color: echarts.textColor,
            },
          },
          grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true,
          },
          xAxis: [
            {
              type: 'category',
              boundaryGap: false,
              data: this.chartData.xAxis,
              axisTick: {
                alignWithLabel: true,
              },
              axisLine: {
                lineStyle: {
                  color: echarts.axisLineColor,
                },
              },
              axisLabel: {
                textStyle: {
                  color: echarts.textColor,
                },
              },
            },
          ],
          yAxis: [
            {
              type: 'value',
              axisLine: {
                lineStyle: {
                  color: echarts.axisLineColor,
                },
              },
              splitLine: {
                lineStyle: {
                  color: echarts.splitLineColor,
                },
              },
              axisLabel: {
                textStyle: {
                  color: echarts.textColor,
                },
              },
            },
          ],
          series: [
            {
              name: this.chartData.legend[0],
              type: 'line',
              stack: 'Total amount',
              areaStyle: { normal: { opacity: echarts.areaOpacity } },
              data: this.chartData.series.loanRepayment,
            },
            {
              name: this.chartData.legend[1],
              type: 'line',
              stack: 'Total amount',
              areaStyle: { normal: { opacity: echarts.areaOpacity } },
              data: this.chartData.series.oneOff,
            },
            {
              name: this.chartData.legend[2],
              type: 'line',
              stack: 'Total amount',
              areaStyle: { normal: { opacity: echarts.areaOpacity } },
              data: this.chartData.series.recurrentExpenditure,
            },
            {
              name:this.chartData.legend[3],
              type: 'line',
              stack: 'Total amount',
              areaStyle: { normal: { opacity: echarts.areaOpacity } },
              data: this.chartData.series.savings ,
            },
          ],
        };
      });
    }
  }

  back()
  {
    this.userService.navigateHome()
  }

  ngOnDestroy(): void {
    if (this.themeSubscription)
    {
      this.themeSubscription.unsubscribe();
    }
  }
}
