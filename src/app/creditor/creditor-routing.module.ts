import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CreditorComponent } from './creditor.component';
import { environment } from '../../environments/environment';
import { AuthGuardService } from '../@core/service/authguard.service';
import { ClientsComponent } from './clients/clients.component';
import { ClientHistoryComponent } from './clients-history/client-history.component';
import { RequestsComponent } from './requests/requests.component';
import { OrganisationByIDResolve } from '../@core/resolver/organisation-by-ID.resolver.service';
import { AllTransactionHistoryResolve } from '../@core/resolver/transactionHistory.resolver';
import { UserResolve } from '../@core/resolver/user.resolver';
import { AllWatchdogResolve } from '../@core/resolver/watchdog.resolver';
import { AllOrganisationResolver } from '../@core/resolver/organisation.resolver';
import { CurrentOrganisationResolver } from '../@core/resolver/currentOrganisation.resolver';
import { UserDetailsResolver } from '../@core/resolver/userDetailsResolver.resolver';
import { SettingComponent } from './setting/setting.component';
import { UserConfigResolve } from '../@core/resolver/userConfig.resolver';
import { AllLoansResolver } from '../@core/resolver/loan.resolver';
import { AllAccountsResolver } from '../@core/resolver/account.resolver';

const routes: Routes = [
  {
  path: 'creditor',
  component: CreditorComponent,
  canActivateChild: [AuthGuardService],
  children: [
    {
      path: 'dashboard',
      component: DashboardComponent,
      data: { allowedRoles:[environment.accountType.creditor] },
      resolve:{
        allTransHist:AllTransactionHistoryResolve,
        allWatchdogs:AllWatchdogResolve,
        allOrgs:AllOrganisationResolver,
        allUsers:UserResolve,
        userConfig:UserConfigResolve,
        allLoans:AllLoansResolver,
        allAccounts:AllAccountsResolver
      },
      pathMatch: 'full'
    },
    {
      path: 'requests',
      component: RequestsComponent,
      data: { allowedRoles:[environment.accountType.creditor] },
      pathMatch: 'full'
    },
    {
      path: 'clients',
      component: ClientsComponent,
      data: { allowedRoles:[environment.accountType.creditor] },
      resolve:{
        allAccounts:AllAccountsResolver,
        transactionHistory: AllTransactionHistoryResolve,
        allUsers:UserResolve,
        allOrgs: AllOrganisationResolver,
        userConfig:UserConfigResolve,
        allLoans:AllLoansResolver,
      },
      pathMatch: 'full'
    },
    {
      path: 'client_details/:id',
      component: ClientHistoryComponent,
      data: { allowedRoles:[environment.accountType.creditor] },
      resolve:{
        organisationData:OrganisationByIDResolve,
        allTransHist:AllTransactionHistoryResolve,
        allUsers: UserResolve,
        userConfig:UserConfigResolve,
        allLoans:AllLoansResolver,
        allAccounts:AllAccountsResolver
      },
      pathMatch: 'full'
    },

    {
      path: 'setting',
      component: SettingComponent,
      data: { allowedRoles:[environment.accountType.creditor] },
      resolve:{
        userData: UserResolve,
        userConfig:UserConfigResolve
      },
      pathMatch: 'full'
    },


    {
      path: '',
      pathMatch:'full',
      redirectTo:'dashboard',
      data: { allowedRoles:[environment.accountType.creditor] }
    },
  ],
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers:[
    OrganisationByIDResolve,
    AllTransactionHistoryResolve,
    UserResolve,
    AllWatchdogResolve,
    AllOrganisationResolver,
    CurrentOrganisationResolver,
    UserDetailsResolver,
    UserConfigResolve,
    AllLoansResolver,
    AllAccountsResolver,
  ]
})
export class CreditorRoutingModule { }
