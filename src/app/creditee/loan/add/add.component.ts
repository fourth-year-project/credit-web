import { Component, OnInit } from '@angular/core';
import { IOrganisation } from '../../../@core/data/organisation';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { IUser } from '../../../@core/data/users';
import { Router } from '@angular/router';
import { NbDateService } from '@nebular/theme';
import { HelperService, NotificationService } from '../../../@core/utils';
import { OrganisationService } from '../../../@core/service/organisation.service';
import { AccountService } from '../../../@core/service/account.service';
import { UserService } from '../../../@core/service/user.service';
import { environment } from '../../../../environments/environment';
import { LoanService } from '../../../@core/service/loans.service';
import { ILoan } from '../../../@core/data/loan';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';

@Component({
  selector: 'add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddLoanComponent implements OnInit {

  min: Date;
  max: Date;
  transactionPeriodStartDate = new Date();
  transactionPeriodEndDate = new Date();

  organisation : IOrganisation
  loanForm: FormGroup
  user:IUser

  notAllowed :boolean

  accountsList:any[] = []
  loanID:number

  constructor(
    private fb: FormBuilder,
    private router:Router,
    protected dateService: NbDateService<Date>,
    private helperService : HelperService,
    private accountService: AccountService,
    private userService: UserService,
    private loanService:LoanService,
    private notificationService:NotificationService,
  ) {
    this.min = this.dateService.addDay(this.dateService.today(), -5);
    this.max = this.dateService.addDay(this.dateService.today(), 5);

   }

   ngOnInit()
   {
    this.createForm();


    this.userService.getUser().subscribe(res=>{
        this.userService.getUserById(res._uuid).subscribe( (usr_res:any)=>{
            this.user = usr_res
            console.log("User details: " +  JSON.stringify(this.user) )

            this.accountService.getAllAccounts().subscribe( (acc_res:any)=>{
              Object.keys(acc_res).forEach(item=>{
                // console.log("AllAccounts item: " +  JSON.stringify(acc_res[item]) )

                if (acc_res[item]._holder == this.user._organisation && acc_res[item]._active == true )
                {
                  this.accountsList.push(acc_res[item])
                }
              })

              // Render error
              if( this.accountsList.length < 1)
              {
                this.notAllowed = true
                console.log("Accounts Count " + this.accountsList.length)
                this.notificationService.makeToast("Add Loan","Not possible due to lack of active accounts",NbToastStatus.WARNING,true)
              }
              else
              {
                this.notAllowed= false
              }
          })
        })
    })

    this.loanService.getAllLoans().subscribe(res=>{
      this.loanID = res.length
    })

   }

   back()
   {
     this.router.navigate([ environment.appRoutes.creditee + environment.crediteeRoutes.loan])
   }

   createForm()
   {
     this.loanForm = this.fb.group({
         '_issueDate': new FormControl('',
         [
           Validators.required,
         ]),

         '_account': new FormControl('',
         [
           Validators.required,
         ]),
         '_amount': new FormControl('',
         [
           Validators.required,
           Validators.pattern(environment.idNumberRegex)
         ]),
         '_repaymentPeriod': new FormControl('',
         [
           Validators.required,
           Validators.max(99),
           Validators.min(1)
         ]),
       })
   }


   onSubmit()
   {
     const result: ILoan = Object.assign({}, this.loanForm.value);

    //  result._id = "l_" + (this.loanID + 10).toString()
     result._recepient = this.user._organisation
     result._issueConfirmation = false
     result._extensionStatus = false
     result._extensionPeriod = 0
     result._paymentConfirmation = false
     result._paymentConfirmationDate = ""
     result._modified = this.helperService.convertDateToString(new Date)
     result._created = this.helperService.convertDateToString(new Date)

     this.loanService.getLoanList().subscribe( (res:any[])=>{

       result._id = "l_" +(res.length + 5).toString()
       console.log("ILoan" + JSON.stringify ( result ) )

       this.loanService.createLoan(result)
    })


   }

}
