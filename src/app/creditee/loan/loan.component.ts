import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WatchDogService } from '../../@core/service/watchdog.service';
import { OrganisationService } from '../../@core/service/organisation.service';
import { TransactionHistoryService } from '../../@core/service/transactionhistory.service';
import { UserService } from '../../@core/service/user.service';
import { IUser } from '../../@core/data/users';
import { LoanService } from '../../@core/service/loans.service';
import { IOrganisation } from '../../@core/data/organisation';
import { AccountService } from '../../@core/service/account.service';
import { ILoan } from '../../@core/data/loan';
import { IAccount, AccountData } from '../../@core/data/account';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'creditee-loan',
  templateUrl: './loan.component.html',
})
export class LoanComponent implements OnInit{

  cardData = {
    items: [],
    placeholders: [],
    loading: false,
    pageToLoadNext: 1,
  };

  pageSize = 1;

  user: IUser;
  loanList: any[] = []
  loanCurrentItemList: any[] = []
  accountsList: any[] = []

  orgDetails:IOrganisation;

  constructor(
    private router:Router,
    private loanService:LoanService,
    private AccountService: AccountData,
    private userService: UserService)
  {}

  ngOnInit()
  {
    this.AccountService.getAllAccounts().subscribe(res=>{
      this.accountsList =res
      // console.log("(LoanComponent) Accounts List" + JSON.stringify(this.accountsList) )

  })
  }

  addLoan()
  {
    this.router.navigate([ environment.appRoutes.creditee + environment.crediteeRoutes.addLoan])
  }

  loadNext(cardData) {
    if (cardData.loading) { return; }

    cardData.loading = true;
    cardData.placeholders = new Array(this.pageSize);

    let loanList : ILoan[] = [];


    this.userService.getUser().subscribe(res=>{
        this.userService.getUserById(res._uuid).subscribe( (usr_res:any)=>{
            this.user = usr_res
            console.log("(LoanComponent): Current User: "+ JSON.stringify(this.user) )

            this.loanService.getAllLoans().subscribe(res=>
              {
                // console.log("(LoanComponent) Loan List" + JSON.stringify(res) )

                Object.keys(res).forEach(item=>
                  {
                    if ( res[item]._recepient == this.user._organisation )
                    {
                      this.loanList.push(res[item])
                      // console.log("(LoanComponent): LoanHist [list]:"+ JSON.stringify(this.loanList) )

                      Object.keys(this.loanList).forEach(item=>
                      {
                        console.log("(AccountComponent): LoanList [item]:"+ JSON.stringify(this.loanList[item]) )

                        if(this.loanCurrentItemList.indexOf(this.loanList[item]._id) === -1 )
                        {
                          loanList.length = 0


                          Object.keys(this.accountsList).forEach(acc_item=>{

                            console.log("(AccountComponent): AccList [item]:"+ JSON.stringify(this.accountsList[acc_item]) )

                            if(this.accountsList[acc_item]._id ==this.loanList[item]._account )
                            {
                              this.loanList[item]._institutionName = this.accountsList[acc_item]._institutionName
                              this.loanList[item]._accountName =  this.accountsList[acc_item]._accountName
                              this.loanList[item]._accountID = this.accountsList[acc_item]._accountID
                              this.loanList[item]._active = this.accountsList[acc_item]._active
                            }
                          })

                          this.loanCurrentItemList.push(this.loanList[item]._id)
                          console.log("(LoanComponent) Added item [Display List]:" + JSON.stringify(this.loanList[item]._id))

                          loanList.push(this.loanList[item])

                          cardData.placeholders = [];
                          cardData.items.push(...loanList);
                          cardData.loading = false;
                          cardData.pageToLoadNext++;
                        }

                        // console.log("(LoanComponent): carddata " + JSON.stringify(cardData.items) )
                        // console.log("(LoanComponent): currentList" + JSON.stringify(this.loanCurrentItemList) )
                        })
                      }
                    })
            })
        })
    })
  }
}
