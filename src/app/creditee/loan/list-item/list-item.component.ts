import { Component, OnInit, Input } from '@angular/core';
import { ILoan } from '../../../@core/data/loan';

@Component({
  selector: 'creditee-loan-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {

  @Input() loanData: ILoan;

  constructor() { }

  ngOnInit() {
  }

}
