import { NgModule } from '@angular/core';
import { ListItemComponent } from './list-item/list-item.component';
import { ThemeModule } from '../../@theme/theme.module';
import { LoanComponent } from './loan.component';
import { AddLoanComponent } from './add/add.component';


const COMPONENTS = [
  LoanComponent,
  AddLoanComponent,
  ListItemComponent,
];
const ENTRY_COMPONENTS = [
];

const MODULES = [
  ThemeModule,
];

const SERVICES = [
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})

export class LoanModule { }
