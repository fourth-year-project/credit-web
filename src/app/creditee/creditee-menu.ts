import { NbMenuItem } from '@nebular/theme';

export const CREDITEE_MENU_ITEMS: NbMenuItem[] = [

  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: 'dashboard',
    home: true,
  },
  {
    title: 'Watchdogs',
    icon: 'ion-ionic',
    link: 'watchdogs',
  },
  {
    title: 'Transactions',
    icon: 'nb-compose',
    link: 'transactions',
  },
  {
    title: 'Accounts',
    icon: 'nb-compose',
    link: 'accounts',
  },
  {
    title: 'Loans',
    icon: 'nb-compose',
    link: 'loans',
  },
];
