import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrediteeComponent } from './creditee.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { environment } from '../../environments/environment';
import { AuthGuardService } from '../@core/service/authguard.service';
import { WatchdogsComponent } from './watchdogs/watchdogs.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { AddComponent } from './transactions/add/add.component';
import { CurrentOrganisationResolver } from '../@core/resolver/currentOrganisation.resolver';
import { AllTransactionHistoryResolve } from '../@core/resolver/transactionHistory.resolver';
import { UserDetailsResolver } from '../@core/resolver/userDetailsResolver.resolver';
import { CurrentUserResolver } from '../@core/service/currentUser.resolver';
import { AllOrganisationResolver } from '../@core/resolver/organisation.resolver';
import { UserResolve } from '../@core/resolver/user.resolver';
import { AccountComponent } from './account/account.component';
import { AddAccountComponent } from './account/add/add.component';
import { AddLoanComponent } from './loan/add/add.component';
import { LoanComponent } from './loan/loan.component';

const routes: Routes = [
  {
    path: 'creditee',
    component: CrediteeComponent,
    canActivateChild: [AuthGuardService],
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
        data: { allowedRoles:[environment.accountType.creditee] },
        resolve:{
          allTransHist:AllTransactionHistoryResolve,
          allUsers:UserResolve,
          allOrgs: AllOrganisationResolver,
        },
        pathMatch: 'full'
      },
      {
        path: 'watchdogs',
        component: WatchdogsComponent,
        data: { allowedRoles:[environment.accountType.creditee] },
        resolve:{
          allUsers:UserResolve,
          allOrgs: AllOrganisationResolver,
        },
        pathMatch: 'full'
      },
      {
        path: 'transactions',
        component: TransactionsComponent,
        data: { allowedRoles:[environment.accountType.creditee] },
        pathMatch: 'full',
      },
      {
        path: 'add_transaction',
        component: AddComponent,
        data: { allowedRoles:[environment.accountType.creditee] },
        pathMatch: 'full'
      },
      {
        path: 'accounts',
        component: AccountComponent,
        data: { allowedRoles:[environment.accountType.creditee] },
        pathMatch: 'full',
      },
      {
        path: 'add_account',
        component: AddAccountComponent,
        data: { allowedRoles:[environment.accountType.creditee] },
        pathMatch: 'full'
      },
      {
        path: 'loans',
        component: LoanComponent ,
        data: { allowedRoles:[environment.accountType.creditee] },
        pathMatch: 'full',
      },
      {
        path: 'add_loan',
        component: AddLoanComponent,
        data: { allowedRoles:[environment.accountType.creditee] },
        pathMatch: 'full'
      },
      {
        path: '',
        pathMatch:'full',
        redirectTo:'dashboard',
        data: { allowedRoles:[environment.accountType.creditee] }
      },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers:[
    AllTransactionHistoryResolve,
    CurrentOrganisationResolver,
    UserResolve,
    CurrentUserResolver,
    UserDetailsResolver,
    AllOrganisationResolver
  ]
})
export class CrediteeRoutingModule { }
