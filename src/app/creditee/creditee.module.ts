import { NgModule } from '@angular/core';

import { CrediteeRoutingModule } from './creditee-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { CrediteeComponent } from './creditee.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { TransactionsModule } from './transactions/transactions.module';
import { WatchdogsModule } from './watchdogs/watchdogs.module';
import { AccountModule } from './account/account.module';
import { LoanModule } from './loan/loan.module';

const CREDITEE_COMPONENTS = [
  CrediteeComponent,
];


@NgModule({
  imports: [
    CrediteeRoutingModule,
    ThemeModule,

    AccountModule,
    DashboardModule,
    LoanModule,
    TransactionsModule,
    WatchdogsModule,
  ],
  declarations: [
    ...CREDITEE_COMPONENTS,
  ],
})

export class CrediteeModule { }
