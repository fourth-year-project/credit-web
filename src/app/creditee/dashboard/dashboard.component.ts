import { Component, OnDestroy, AfterViewInit, OnInit } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { UserService } from '../../@core/service/user.service';
import { IUser } from '../../@core/data/users';
import { HelperService } from '../../@core/utils/helper.service';
import { IOrganisation } from '../../@core/data/organisation';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'creditee-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit, AfterViewInit,OnDestroy {

  results = [
    {
      name: 'recurrentExpenditure',
      value: 0,
    },
    {
      name: 'loanRepayment',
      value: 0,
    },
    {
      name: 'savingAmount',
      value: 0,
    },
  ];

  userDetails = <IUser>{}
  orgDetails = <IOrganisation>{}
  allTransHistList : any[] = []
  allOrgsList:any[] = []
  allUserList:any[] = []

  options: any = {};

  currentDebt: any = []
  loanRepayment:any = []
  transactionDate: any = []

  colorScheme: any;
  themeSubscription: any;

  ChartDataReady:boolean

  user: IUser;
  transHistoryCurrentItemList: any[] = []

  constructor(
      private theme: NbThemeService,
      private route: ActivatedRoute,
      private userService: UserService,
      private helperService: HelperService)
    {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      this.colorScheme = {
        domain: [colors.primaryLight, colors.infoLight, colors.successLight, colors.warningLight, colors.dangerLight],
      };
    });

    this.ChartDataReady =false
    this.userService.getUser().subscribe(res=>{
      this.user = res
    })

  }

  ngOnInit()
  {
    this.allUserList = this.route.snapshot.data.allUsers
    this.allOrgsList = this.route.snapshot.data.allOrgs
    this.allTransHistList = this.route.snapshot.data.allTransHist

    Object.keys(this.allUserList).forEach(element => {

        if (this.allUserList[element]._uuid == this.userService.currentUserValue()._uuid)
        {
          this.user = this.allUserList[element]
        }
    });

    Object.keys(this.allOrgsList).forEach(element=>
      {
        if(this.allOrgsList[element]._id == this.user._organisation )
        {
          this.orgDetails = this.allOrgsList[element]
        }
      })

    // console.log("(DashboarComponent): Test allUsers: " + JSON.stringify( this.route.snapshot.data) )


    Object.keys(this.allTransHistList).forEach(item=>
      {
        if (this.allTransHistList[item]._organisationID == this.orgDetails._id )
        {
          if(this.transHistoryCurrentItemList.indexOf(this.allTransHistList[item]._id) === -1 )
          {
            this.transHistoryCurrentItemList.push(this.allTransHistList[item]._id)
            console.log("(DashboardComponent) Added item [Display List]:" + JSON.stringify(this.allTransHistList[item]))

            this.results[0].value += this.allTransHistList[item]._recurrentExpenditure
            this.results[1].value += this.allTransHistList[item]._loanRepayment
            this.results[2].value += this.allTransHistList[item]._savingAmount

            this.currentDebt.push(this.allTransHistList[item]._currentDebt)
            this.loanRepayment.push(this.allTransHistList[item]._loanRepayment )
            this.transactionDate.push( this.helperService.convertDateForDisplay (this.allTransHistList[item]._transactionPeriodEnd ))

            this.ChartDataReady  = true
            console.log("(DashboardComponent): Health Results:"+ JSON.stringify(this.results) )
          }
        }
      })
  }

  ngAfterViewInit() {

    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const echarts: any = config.variables.echarts;

      this.options = {
        backgroundColor: echarts.bg,
        color: [colors.warningLight, colors.infoLight, colors.dangerLight, colors.successLight, colors.primaryLight],
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'cross',
            label: {
              backgroundColor: echarts.tooltipBackgroundColor,
            },
          },
        },
        legend: {
          data: ['Current Debt','Loan Repayment'],
          textStyle: {
            color: echarts.textColor,
          },
        },
        grid: {
          left: '3%',
          right: '4%',
          bottom: '3%',
          containLabel: true,
        },
        xAxis: [
          {
            type: 'category',
            boundaryGap: false,
            data: this.transactionDate,
            axisTick: {
              alignWithLabel: true,
            },
            axisLine: {
              lineStyle: {
                color: echarts.axisLineColor,
              },
            },
            axisLabel: {
              textStyle: {
                color: echarts.textColor,
              },
            },
          },
        ],
        yAxis: [
          {
            type: 'value',
            axisLine: {
              lineStyle: {
                color: echarts.axisLineColor,
              },
            },
            splitLine: {
              lineStyle: {
                color: echarts.splitLineColor,
              },
            },
            axisLabel: {
              textStyle: {
                color: echarts.textColor,
              },
            },
          },
        ],
        series: [
          {
            name: 'Current Debt',
            type: 'line',
            stack: 'Total amount',
            areaStyle: { normal: { opacity: echarts.areaOpacity } },
            data: this.currentDebt,
          },
          {
            name: 'Loan Repayment',
            type: 'line',
            stack: 'Total amount',
            areaStyle: { normal: { opacity: echarts.areaOpacity } },
            data: this.loanRepayment,
          },
        ],
      };
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
