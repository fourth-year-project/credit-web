import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';

import { NgxEchartsModule } from 'ngx-echarts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartModule } from 'angular2-chartjs';

@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule, 
    NgxChartsModule, 
    ChartModule
  ],
  declarations: [
    DashboardComponent,
  ]
})
export class DashboardModule { }
