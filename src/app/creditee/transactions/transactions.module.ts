import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { TransactionsComponent } from './transactions.component';
import { AddComponent } from './add/add.component';
import { ListItemComponent } from './list-item/list-item.component';
import { NgxEchartsModule } from 'ngx-echarts';

const COMPONENTS = [
  TransactionsComponent,
  AddComponent,
  ListItemComponent,
];
const ENTRY_COMPONENTS = [
];

const MODULES = [
  ThemeModule,
  NgxEchartsModule
];

const SERVICES = [
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})

export class TransactionsModule { }
