import { Component, OnInit, Input, AfterViewInit, OnDestroy } from '@angular/core';
import { ITransactionHistory } from '../../../@core/data/transactionHistory';
import { NbThemeService } from '@nebular/theme';

@Component({
  selector: 'creditee-transactions-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements AfterViewInit, OnDestroy {

    @Input() transactionHistory: ITransactionHistory;

    options: any = {};
    themeSubscription: any;

    constructor(private theme: NbThemeService) {
    }

    ngAfterViewInit() {
      this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

        const colors = config.variables;
        const echarts: any = config.variables.echarts;

        this.options = {
          backgroundColor: echarts.bg,
          color: [colors.warningLight, colors.infoLight, colors.dangerLight, colors.successLight, colors.primaryLight],
          tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b} : {c} ({d}%)',
          },
          legend: {
            orient: 'vertical',
            left: 'left',
            data: ['Recurrent Expenditure', 'Loan Repayment', 'Saving Amount'],
            textStyle: {
              color: echarts.textColor,
            },
          },
          series: [
            {
              name: 'Distribution',
              type: 'pie',
              radius: '80%',
              center: ['50%', '50%'],
              data: [
                { value: this.transactionHistory._recurrentExpenditure, name: 'Recurrent Expenditure' },
                { value: this.transactionHistory._loanRepayment, name: 'Loan Repayment' },
                { value: this.transactionHistory._savingAmount, name: 'Saving Amount' },
              ],
              itemStyle: {
                emphasis: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: echarts.itemHoverShadowColor,
                },
              },
              label: {
                normal: {
                  textStyle: {
                    color: echarts.textColor,
                  },
                },
              },
              labelLine: {
                normal: {
                  lineStyle: {
                    color: echarts.axisLineColor,
                  },
                },
              },
            },
          ],
        };
      });
    }

    ngOnDestroy(): void {
      this.themeSubscription.unsubscribe();
    }
  }
