import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WatchDogService } from '../../@core/service/watchdog.service';
import { OrganisationService } from '../../@core/service/organisation.service';
import { TransactionHistoryService } from '../../@core/service/transactionhistory.service';
import { UserService } from '../../@core/service/user.service';
import { IUser } from '../../@core/data/users';
import { ITransactionHistory } from '../../@core/data/transactionHistory';

@Component({
  selector: 'creditee-transactions',
  templateUrl: './transactions.component.html',
})
export class TransactionsComponent implements OnInit{

  cardData = {
    items: [],
    placeholders: [],
    loading: false,
    pageToLoadNext: 1,
  };

  pageSize = 1;

  user: IUser;
  transactionHistoryList: any[] = []
  transHistoryCurrentItemList: any[] = []

  constructor(private router:Router,
    private transHistService: TransactionHistoryService,
    private userService: UserService)
  {}

  ngOnInit()
  {
    this.userService.getUser().subscribe(res=>{
      this.userService.getUserById(res._uuid).subscribe( (usr_res:any)=>{
        this.user = usr_res
        console.log("(TransactionComponent): Current User:"+ JSON.stringify(this.user) )
      })

    })
  }

  addTransaction()
  {
    this.router.navigate(['/creditee/add_transaction'])
  }

  loadNext(cardData) {
    if (cardData.loading) { return; }

    cardData.loading = true;
    cardData.placeholders = new Array(this.pageSize);

    console.log("(TransactionComponent): In card Data:"+ JSON.stringify(this.user) )

    let transHistoryList : ITransactionHistory[] = [];

    var prevSaleTemp = 0
    var prevDebtTemp = 0

    this.transHistService.getAllTransactionHistory().subscribe(res=>
      {
        Object.keys(res).forEach(item=>
          {
            // console.log("(TransactionComponent): transHist [list]:"+ JSON.stringify(res) )
            if (res[item]._organisationID == this.user._organisation )
            {
              this.transactionHistoryList.push(res[item])
              // console.log("(TransactionComponent): transHist [list]:"+ JSON.stringify(this.transactionHistoryList)
              Object.keys(this.transactionHistoryList).forEach(item=>
              {
                if(this.transHistoryCurrentItemList.indexOf(this.transactionHistoryList[item]._id) === -1 )
                {
                  transHistoryList.length = 0

                  this.transHistoryCurrentItemList.push(this.transactionHistoryList[item]._id)
                  // console.log("(transactionComponent) Added item [Display List]:" + JSON.stringify(this.transactionHistoryList[item]._id))


                    var saleChangetemp =  Number(this.transactionHistoryList[item]._amountIn) - prevSaleTemp
                    var saleCalc
                    if(this.transactionHistoryList[item]._amountIn ==0 )
                    {
                      saleCalc = 1
                    }
                    else
                    {
                      saleCalc = this.transactionHistoryList[item]._amountIn
                    }
                    var saleChange = {
                        value: Math.round((saleChangetemp/ Number(this.transactionHistoryList[item]._amountIn)) *100),
                        sign: (Math.sign(saleChangetemp)  < 0 ? false: true )
                    }

                    var debtChangetemp = Number(this.transactionHistoryList[item]._currentDebt) - prevDebtTemp

                    var debtCalc
                    if(this.transactionHistoryList[item]._currentDebt ==0 )
                    {
                      debtCalc = 1
                    }
                    else
                    {
                      debtCalc = this.transactionHistoryList[item]._currentDebt
                    }
                    var debtChange = {
                        value:Math.round( (debtChangetemp/ Number(debtCalc)) *100),
                        sign: (Math.sign(debtChangetemp) < 0 ? false: true )
                    }

                    console.log( "debt change " + debtChangetemp +" - " +prevDebtTemp +  " =>" + this.transactionHistoryList[item]._currentDebt )

                    prevSaleTemp = Number(this.transactionHistoryList[item]._amountIn)
                    prevDebtTemp = this.transactionHistoryList[item]._currentDebt


                  this.transactionHistoryList[item].saleChange = saleChange
                  this.transactionHistoryList[item].debtChange = debtChange

                  transHistoryList.push(this.transactionHistoryList[item])

                  cardData.placeholders = [];
                  cardData.items.push(...transHistoryList);
                  cardData.loading = false;
                  cardData.pageToLoadNext++;
                }

                // console.log("(transactionComponent): carddata " + JSON.stringify(cardData.items) )
                // console.log("(transactionComponent): currentList" + JSON.stringify(this.transHistoryCurrentItemList) )
                })
              }
            })
        })
  }
}
