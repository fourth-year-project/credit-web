import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl, ValidatorFn } from '@angular/forms';
import { TransactionHistoryService } from '../../../@core/service/transactionhistory.service';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';
import { NbDateService } from '@nebular/theme';
import { transactionHistoryFiguresValidator } from '../../../@core/validators/transactionHistoryFiguresValidator';
import { HelperService } from '../../../@core/utils/helper.service';
import { ITransactionHistory } from '../../../@core/data/transactionHistory';
import { OrganisationService } from '../../../@core/service/organisation.service';
import { IOrganisation } from '../../../@core/data/organisation';
import { transactionPeriodValidator } from '../../../@core/validators/transactionPeriod.validator';
import { UserService } from '../../../@core/service/user.service';
import { IUser } from '../../../@core/data/users';

@Component({
  selector: 'add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  min: Date;
  max: Date;
  transactionPeriodStartDate = new Date();
  transactionPeriodEndDate = new Date();

  organisation : IOrganisation
  transactionForm: FormGroup
  user:IUser

  transCount:number = 0

  constructor(
      private fb: FormBuilder,
      private transactionService: TransactionHistoryService,
      private router:Router,
      protected dateService: NbDateService<Date>,
      private helperService : HelperService,
      private organisationService: OrganisationService,
      private userService: UserService
    ) {
      this.min = this.dateService.addDay(this.dateService.today(), -5);
      this.max = this.dateService.addDay(this.dateService.today(), 5);

     }

  ngOnInit() {
    this.createForm();

    this.userService.getUserById(this.userService.currentUserSubject.value._uuid).subscribe(res=>{
      this.user = res
      console.log("User details: " +  JSON.stringify(this.user) )

        this.organisationService.getOrganisationByID(this.user._organisation).subscribe(res=>{
          this.organisation =res
          console.log("Org Details: " +  JSON.stringify(this.organisation) )

            this.transactionService.getAllTransactionHistory().subscribe( (res:any[])=>{
              this.transCount= res.length

            })
        })
    })
  }

  back()
  {
    this.router.navigate([ environment.appRoutes.creditee + environment.crediteeRoutes.transaction])
  }
  createForm()
  {
    this.transactionForm = this.fb.group({
        'amountIn': new FormControl('',
        [
          Validators.required,
        ],
        [
          transactionHistoryFiguresValidator()
        ]),
        'oneOffCost': new FormControl('',
        [
          Validators.required,
        ],
        [
          transactionHistoryFiguresValidator()
        ]),
        'loanRepayment': new FormControl('',
        [
          Validators.required,
        ],
        [
          transactionHistoryFiguresValidator()
        ]),
        'recurrentExpenditure': new FormControl('',
        [
          Validators.required,
        ],
        [
          transactionHistoryFiguresValidator()
        ]),
        'savingAmount': new FormControl('',
        [
          Validators.required,
        ],
        [
          transactionHistoryFiguresValidator()
        ]),
        'transactionPeriodStart': new FormControl('',
        [
          Validators.required,
          this.validateDate
        ]),
        'transactionPeriodEnd': new FormControl('',
        [
          Validators.required,
          this.validateDate
        ]),
      }
      )
  }


  validateDate(AC: FormControl) {
    let dataForm = AC.parent;

    if(!dataForm) return null;

    let transactionPeriodStart = new Date(dataForm.get('transactionPeriodStart').value );
    let transactionPeriodEnd = new Date(dataForm.get('transactionPeriodEnd').value);


    if( transactionPeriodStart > transactionPeriodEnd) {
      console.log("ValidateDate error start date " )

      dataForm.controls["transactionPeriodEnd"].setErrors( { dateError: true} );
      dataForm.controls["transactionPeriodStart"].setErrors( { dateError: true} );

      if( dataForm.get('transactionPeriodEnd').value == AC ) {
        return {transactionPeriodEnd: {dateError: true} };
      }
      else if(dataForm.get('transactionPeriodStart').value == AC ) {
        return {transactionPeriodStart: {dateError: true} };
      }
    } else {
      dataForm.controls["transactionPeriodEnd"].setErrors( null );
    }
    return null;
  }


  onSubmit()
  {
    const result: any = Object.assign({}, this.transactionForm.value);

    result._id = "th_" + (this.transCount +10).toString()
    result._organisationID =this.user._organisation
    result._amountIn = this.transactionForm.value.amountIn
    result._currentDebt = 0
    result._oneOffCost = this.transactionForm.value.oneOffCost
    result._loanRepayment = this.transactionForm.value.loanRepayment
    result._recurrentExpenditure= this.transactionForm.value.recurrentExpenditure
    result._savingAmount = this.transactionForm.value.savingAmount
    result._transactionPeriodEnd = new Date(this.transactionForm.value.transactionPeriodEnd).toISOString()
    result._transactionPeriodStart = new Date(this.transactionForm.value.transactionPeriodStart).toISOString()

    result._modified = (new Date).toISOString()
    result._created = (new Date).toISOString()

    console.log("ITransHist" + JSON.stringify ( result ) )

    this.transactionService.createTransactionHistory(result)
  }

}
