import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { WatchdogsComponent } from './watchdogs.component';
import { AddDialogComponent } from './add-dialog/add-dialog.component';
import { NbDialogModule, NbWindowModule } from '@nebular/theme';
import { ListItemComponent } from './list-item/list-item.component';

const COMPONENTS = [
  WatchdogsComponent,
  AddDialogComponent,
  ListItemComponent,
];
const ENTRY_COMPONENTS = [
  AddDialogComponent
];

const MODULES = [
  ThemeModule,
  NbDialogModule.forChild(),
  NbWindowModule.forChild(),
];

const SERVICES = [
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,

  ],
  providers: [
    ...SERVICES,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})
export class WatchdogsModule { }
