import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IOrganisation } from '../../../@core/data/organisation';

@Component({
  selector: 'creditee-watchdog-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent {

  @Input() watchdog: IOrganisation;
  @Output() receiveWatchdogRemoveEvent = new EventEmitter<string>();

  constructor() { }

  sendMessage(message:string ) {
    this.receiveWatchdogRemoveEvent.emit( message)
  }

}
