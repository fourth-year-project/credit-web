import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { WatchDogService } from '../../../@core/service/watchdog.service';
import { OrganisationService } from '../../../@core/service/organisation.service';
import { IOrganisation } from '../../../@core/data/organisation';
import { UserService } from '../../../@core/service/user.service';
import { IUser } from '../../../@core/data/users';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NotificationService } from '../../../@core/utils/notification.service';

@Component({
  selector: 'add-dialog',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.scss']
})
export class AddDialogComponent implements OnInit {

  addWatchdogForm : FormGroup
  selectedItemFormControl = new FormControl();

  userDetails = <IUser>{}

  organisationDetails=<IOrganisation>{};
  organisationList : any[] = []
  watchdogsList : any[] =[]
  currentWatchdogsList:any[] =[]
  result:any[]

  dogs:Array<IOrganisation> = []
  currentDogsList:any =[]

  constructor(
    protected ref: NbDialogRef<AddDialogComponent>,
    private fb: FormBuilder,
    private watchdogService: WatchDogService,
    private organisationService: OrganisationService,
    private userService: UserService,
    public  notificationService:NotificationService
    ) {}


  loadData()
  {
    this.watchdogService.getAllWatchDogs().subscribe(res=>
      {
        console.log("{Add-DialogComponent} - (getAllWatchdogs):[list] : " +JSON.stringify(res) )
        this.watchdogService.getWatchdogsList().subscribe(res=>{
          this.watchdogsList =res

          // List of current watchdogs
          Object.keys(this.watchdogsList).forEach(wd_item=>
            {
              if (this.organisationDetails._id  == this.watchdogsList[wd_item]._authorizingOrganization && this.watchdogsList[wd_item]._active)
              {
                this.currentWatchdogsList.push (this.watchdogsList[wd_item]._authorizedOrganization)
                console.log("{Add-DialogComponent} - (currentWatchdogList):[list] : " +JSON.stringify(this.currentWatchdogsList) )

              }
          })

          // Filter
          Object.keys(this.organisationList).forEach(org_item=>
            {
              if( this.currentWatchdogsList.indexOf(this.organisationList[org_item]._id) === -1)
              {
                if (this.currentDogsList.indexOf( this.organisationList[org_item]._id) === -1 )
                {
                  if (this.organisationList[org_item]._businessType == "creditor")
                  {
                    this.dogs.push(this.organisationList[org_item])
                    this.currentDogsList.push(this.organisationList[org_item]._id)

                    console.log("Potential added: " + JSON.stringify(this.currentDogsList) )
                  }
                }
              }
            })
            this.notificationService.makeToast("Watchdogs List","loading completed",NbToastStatus.SUCCESS,true)

            console.log("Dogs list" + JSON.stringify(this.dogs) )

        })
        // console.log("{Add-DialogComponent} - (getAllWatchdogList):[list] : " +JSON.stringify(this.watchdogsList) )
        // console.log("{Add-DialogComponent} - (AllWatchdogList):[list] : " +JSON.stringify(this.dogs) )
      })
  }

  display()
  {
    console.log( "{Add-DialogComponent} - Selected component " + this.result)
  }

  ngOnInit()
  {
    this.createForm();
    console.log("(Add-dialog)" + this.addWatchdogForm.value)

    this.organisationService.getAllOrganisations().subscribe(res =>{
      this.organisationService.getOrganisationsList().subscribe(res=>
        {
          this.organisationList = res

        })
        console.log("{Add-DialogComponent} - (getAllOrganisations):[list] : " +JSON.stringify(this.organisationList) )
    })

    this.notificationService.makeToast("Watchdogs List","Loading list. Kindly wait",NbToastStatus.WARNING,true)

    this.userService.getCurrentUserById(this.userService.currentUserValue()._uuid)
        .subscribe(res=>
        {
          this.organisationService.getOrganisationByID(res._organisation)
              .subscribe( org_res =>
              {
                  console.log(" Current user org : "  + JSON.stringify(org_res) )
                  this.organisationDetails = org_res
                  console.log(" Current user org IOrg: "  + JSON.stringify(this.organisationDetails) )

                  this.loadData()
              })
        })

  }

  cancel() {
    this.ref.close()
  }

  onSubmit() {
    console.log("(Add-Watchdog) Submission"+JSON.stringify(this.result))

    // Pass value to calling component
    this.ref.close(this.result);
  }

  createForm()
  {
    this.addWatchdogForm = this.fb.group({
      'watchdog': new FormControl('',
          [
            Validators.required,
          ]),
      })
  }

}
