import { Component, OnInit, ViewChild } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { AddDialogComponent } from './add-dialog/add-dialog.component';
import { WatchDogService } from '../../@core/service/watchdog.service';
import { IOrganisation } from '../../@core/data/organisation';
import { OrganisationService } from '../../@core/service/organisation.service';
import { ILoan } from '../../@core/data/loan';
import { LoanService } from '../../@core/service/loans.service';
import { of as observableOf,  Observable} from 'rxjs';
import { IWatchdog } from '../../@core/data/watchdog';
import { ActivatedRoute } from '@angular/router';
import { IUser } from '../../@core/data/users';
import { UserService } from '../../@core/service/user.service';

@Component({
  selector: 'creditee-watchdogs',
  templateUrl: './watchdogs.component.html',
})

export class WatchdogsComponent implements OnInit
{
  cardData = {
    items: [],
    placeholders: [],
    loading: false,
    pageToLoadNext: 1,
  };

  pageSize = 1;

  loans : Observable<ILoan[]>
  loan : ILoan;
  test : Observable<IWatchdog[]>

  userDetails = <IUser>{}
  orgDetails = <IOrganisation>{}
  allTransHistList : any[] = []
  allOrgsList:any[] = []
  allUserList:any[] = []


  organisationList : any[] =[]
  watchdogsList : any[] =[]
  currentItemsList:IOrganisation[] =[]
  removedWatchdogList:any[] = []

  removeWatchdogItemID:string;

  constructor(
    private dialogService: NbDialogService,
    private watchdogService: WatchDogService,
    private organisationService: OrganisationService,
    private loanService: LoanService,
    private userService: UserService,
    private route:ActivatedRoute
    ) {
      var indices = [];
    }


  ngOnInit()
  {

    this.allUserList = this.route.snapshot.data.allUsers
    this.organisationList = this.route.snapshot.data.allOrgs
    this.allTransHistList = this.route.snapshot.data.allTransHist

    Object.keys(this.allUserList).forEach(element => {

        if (this.allUserList[element]._uuid == this.userService.currentUserValue()._uuid)
        {
          this.userDetails= this.allUserList[element]
        }
    });

    Object.keys(this.organisationList).forEach(element=>
      {
        if(this.organisationList[element]._id == this.userDetails._organisation )
        {
          this.orgDetails = this.organisationList[element]
        }
      })

    console.log(" (WatchdogComponenet) Org details : " + JSON.stringify(this.orgDetails) )

    // this.organisationService.getAllOrganisations().subscribe(res =>{
    //   this.organisationService.getOrganisationsList().subscribe(res=>
    //     {
    //       this.organisationList = res

    //     })
    //     // console.log("{WatchdogComponent} - (getAllOrganisations):[list] : " +JSON.stringify(this.organisationList) )
    // })

    this.watchdogService.getAllWatchDogs().subscribe(res=>
      {
        // console.log("{WatchdogComponent} - (getAllWatchdogs):[list] : " +JSON.stringify(res) )
        this.watchdogService.getWatchdogsList().subscribe(res=>this.watchdogsList =res)
        // console.log("{WatchdogComponent} - (getAllWatchdogList):[list] : " +JSON.stringify(this.watchdogsList) )
      })
  }


  receiveWatchdogRemove($event) {
    this.removeWatchdogItemID = $event
    this.handleRemovedWatchdog($event)
  }

  handleRemovedWatchdog( id:string)
  {
    console.log("(WatchdogComponent) : removed watchdog id: " + id )
    this.watchdogService.cancelWatchdogRights(id)
    // update local lists
    this.removedWatchdogList.push(id)

    // TODO: test carddata
    this.loadNext(this.cardData)

    this.watchdogService.cancelWatchdogRights(id)

  }

  loadNext(cardData) {
    if (cardData.loading) { return; }

    cardData.loading = true;
    cardData.placeholders = new Array(this.pageSize);

    let organisationList : IOrganisation[] = [];

    this.watchdogService.getAllWatchDogs().subscribe(res=>
    {
      this.watchdogsList.length = 0
      this.watchdogsList = res

      Object.keys(this.watchdogsList).forEach(wd_item=>
      {
        if(this.watchdogsList[wd_item]._active )
        {
          Object.keys(this.organisationList).forEach(org_item=>
          {
              if ( this.orgDetails._id == this.watchdogsList[wd_item]._authorizingOrganization && this.watchdogsList[wd_item]._authorizedOrganization == this.organisationList[org_item]._id)
              {
                  if(this.currentItemsList.indexOf(this.organisationList[org_item]._id) === -1 )
                  {
                    if (this.organisationList[org_item]._businessType == 'creditor')
                    {
                      organisationList.length = 0
                      this.currentItemsList.push(this.organisationList[org_item]._id)
                      console.log("Added item :" + JSON.stringify(this.organisationList[org_item]._id))

                      this.organisationList[org_item]._watchdog_id =  this.watchdogsList[wd_item]._id
                      this.organisationList[org_item]._watchdog_authorized =  this.watchdogsList[wd_item]._authorized
                      this.organisationList[org_item]._watchdog_active =  this.watchdogsList[wd_item]._active

                      organisationList.push(this.organisationList[org_item])

                      cardData.placeholders = [];
                      cardData.items.push(...organisationList);
                      cardData.loading = false;
                      cardData.pageToLoadNext++;
                    }
                  }
                  console.log("Current Display org card items :" + JSON.stringify(organisationList) )

                  console.log("Current Display org list :" + JSON.stringify(this.currentItemsList) )

                  // Remove watchdogs from carddata on remove

                  Object.keys(this.removedWatchdogList).forEach(item =>
                  {
                      var index: number = this.cardData.items.indexOf(this.removedWatchdogList[item],0)
                      if( index > -1 )
                      {
                        this.cardData.items.splice(index,1)
                      }
                  })
              }
            })
        }
      })
    })
  }

  addWatchdog() {
    this.dialogService.open(AddDialogComponent)
      .onClose.subscribe(
          watchdogs =>
          {
            console.log("(Watchdog-component) AddComponent List :" + JSON.stringify(watchdogs))

            if(watchdogs != undefined)
            {
                Object.keys(watchdogs).forEach(list_item =>
                {
                  Object.keys(this.organisationList).forEach(item=>
                    {
                      if (this.organisationList[item]._id == watchdogs[list_item] )
                      {
                        console.log("(Watchdog-component) Test : " +this.organisationList[item]._id + " == "+  watchdogs[list_item] )

                        var temp = <IWatchdog>{}
                        var date =  new Date()
                        temp._id = "wdog_0" +(this.watchdogsList.length + 5).toString()
                        temp._authorizingOrganization = this.orgDetails._id
                        temp._authorizedOrganization = this.organisationList[item]._id
                        temp._active = true
                        temp._authorized =  false
                        temp._created = date.toISOString()
                        temp._modified  = date.toISOString()

                        console.log(" Creating Watchdog : " + JSON.stringify(temp) )

                        this.watchdogService.createWatchdog(temp)
                      }
                    })
                })
            }

          }
      );
  }

}
