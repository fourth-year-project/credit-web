import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NbDateService } from '@nebular/theme';
import { HelperService } from '../../../@core/utils';
import { AccountService } from '../../../@core/service/account.service';
import { UserService } from '../../../@core/service/user.service';
import { OrganisationService } from '../../../@core/service/organisation.service';
import { IOrganisation } from '../../../@core/data/organisation';
import { IUser } from '../../../@core/data/users';
import { environment } from '../../../../environments/environment';
import { UtilityService } from '../../../@core/utils/utility.service';

@Component({
  selector: 'add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddAccountComponent implements OnInit {

  min: Date;
  max: Date;
  transactionPeriodStartDate = new Date();
  transactionPeriodEndDate = new Date();

  organisation : IOrganisation
  accountForm: FormGroup
  user:IUser

  accountTypeList:any[] = []
  currentAccountTypeList:any[] =[]
  creditorsList:any[]=[]

  selectedInstitutionName:string=""
  selectedAccountType:string=""

  regNumber:any


  constructor(
    private fb: FormBuilder,
    private router:Router,
    protected dateService: NbDateService<Date>,
    private helperService : HelperService,
    private userService: UserService,
    private utilService:UtilityService,
    private accountService:AccountService
  ) {
    this.min = this.dateService.addDay(this.dateService.today(), -5);
    this.max = this.dateService.addDay(this.dateService.today(), 5);

   }

  ngOnInit() {
    this.createForm();

    this.userService.getUser().subscribe(res=>{
      this.userService.getUserById(res._uuid).subscribe( (usr_res:any)=>{
          this.user = usr_res
          console.log("user details: " +  JSON.stringify(this.user) )
        })
    })

    this.utilService.getAllAccountType().subscribe( (res:any)=>{
        this.accountTypeList=res
        console.log("Account Types:" + JSON.stringify(this.accountTypeList) )
    })

    this.utilService.getAllCreditors().subscribe( (res:any)=>{
      this.creditorsList=res
      console.log("Creditor:" + JSON.stringify(this.creditorsList) )

    })

  }

  back()
  {
    this.router.navigate([ environment.appRoutes.creditee + environment.crediteeRoutes.account])
  }

  createForm()
  {
    this.accountForm = this.fb.group({
        'institution': new FormControl('',
        [
          Validators.required,
        ]),

        'accountName': new FormControl('',
        [
          Validators.required,
        ]),
        'accountID': new FormControl('',
        [
          Validators.required,
          Validators.pattern(environment.accountIDRegex)
        ]),
      })

      // Loop to alter account type by organisation
      this.accountForm.controls.institution.valueChanges.subscribe(change=>
      {
        this.regNumber =change
        console.log()
        this.currentAccountTypeList.length = 0
        Object.keys(this.accountTypeList).forEach(item=>
          {
            if (this.accountTypeList[item].organisation == change )
            {
              this.currentAccountTypeList.push(this.accountTypeList[item])
            }
          })
      })
  }


  onSubmit()
  {
    const result: any = Object.assign({}, this.accountForm.value);

    console.log( JSON.stringify(this.creditorsList) )

    // Loop through list to get institution name
    Object.keys(this.creditorsList).forEach(item=>{
      if (this.creditorsList[item].registrationID == result.institution )
      {
        result._institutionName =this.creditorsList[item].name

        console.log("InstitutionName" + this.creditorsList[item].organisation + " - "+ result.institution + "=>"+ result._institutionName )
      }
    })

    Object.keys(this.accountTypeList).forEach(item=>{
      if (this.accountTypeList[item].organisation == result.institution )
      {
        result._accountType =this.accountTypeList[item].type

        console.log("Account Type" +result._accountType )

      }
    })

    result._id = (result._institutionName).toString().substring(0,3) + "_" + (result.accountID).toString().toString()
    result._institution = result.institution
    result._accountID = (result.accountID).toString()
    result._accountName = result.accountName
    result._accountStatus ="good"
    result._active =true
    result._holder = this.user._organisation
    result._modified = this.helperService.convertDateToString(new Date)
    result._created = this.helperService.convertDateToString(new Date)

    console.log("IAccount" + JSON.stringify ( result ) )

    this.accountService.createAccount(result)
  }

}
