import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IAccount } from '../../../@core/data/account';

@Component({
  selector: 'creditee-account-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent {

  @Input() account: IAccount;
  @Output() passCloseAccountEvent = new EventEmitter<string>();

  constructor() { }

  sendMessage(message:string ) {
    this.passCloseAccountEvent.emit( message)
  }

}
