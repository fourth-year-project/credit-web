import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WatchDogService } from '../../@core/service/watchdog.service';
import { OrganisationService } from '../../@core/service/organisation.service';
import { TransactionHistoryService } from '../../@core/service/transactionhistory.service';
import { UserService } from '../../@core/service/user.service';
import { IUser } from '../../@core/data/users';
import { ITransactionHistory } from '../../@core/data/transactionHistory';
import { AccountService } from '../../@core/service/account.service';

@Component({
  selector: 'creditee-account',
  templateUrl: './account.component.html',
})

// TODO: Test

export class AccountComponent implements OnInit{

  cardData = {
    items: [],
    placeholders: [],
    loading: false,
    pageToLoadNext: 1,
  };

  pageSize = 1;

  user: IUser;
  AccountList: any[] = []
  AccountCurrentItemList: any[] = []

  closeAccountID:string

  constructor(
    private router:Router,
    private accountService:AccountService,
    private userService: UserService)
  {}

  ngOnInit()
  {
    // this.userService.getUser().subscribe(res=>{
    //   this.userService.getUserById(res._uuid).subscribe( (usr_res:any)=>{
    //     this.user = usr_res
    //     console.log("(AccountComponent): Current User: " + JSON.stringify(this.user) )
    //   })
    // })
  }

  addAccount()
  {
    this.router.navigate(['/creditee/add_account'])
  }

  closeAccountEvent($event) {
    var closeAccountID = $event

    this.accountService.closeAccount(closeAccountID)
  }

  loadNext(cardData) {
    if (cardData.loading) { return; }

    cardData.loading = true;
    cardData.placeholders = new Array(this.pageSize);

    let accountList : ITransactionHistory[] = [];

    // console.log("(AccountComponent): In card Data:"+ JSON.stringify(this.user) )

    this.userService.getUser().subscribe(res=>{
        this.userService.getUserById(res._uuid).subscribe( (usr_res:any)=>{
            this.user = usr_res
            this.accountService.getAllAccounts().subscribe(res=>
            {
                Object.keys(res).forEach(item=>
                {
                  if (res[item]._active && res[item]._holder == this.user._organisation)
                  {
                    this.AccountList.push(res[item])
                    // console.log("(AccountComponent): accounts [list]:"+ res[item]._holder + " => " + this.user._organisation)

                    Object.keys(this.AccountList).forEach(item=>
                    {
                      if(this.AccountCurrentItemList.indexOf(this.AccountList[item]._id) === -1 )
                      {
                        accountList.length = 0

                        this.AccountCurrentItemList.push(this.AccountList[item]._id)
                        // console.log("(AccountComponent) Added item [Display List]:" + JSON.stringify(this.AccountList[item]._id))


                        accountList.push(this.AccountList[item])

                        cardData.placeholders = [];
                        cardData.items.push(...accountList);
                        cardData.loading = false;
                        cardData.pageToLoadNext++;
                      }

                      // console.log("(AccountComponent): carddata " + JSON.stringify(cardData.items) )
                      // console.log("(AccountComponent): currentList" + JSON.stringify(this.AccountCurrentItemList) )
                      })
                    }
                  })
              })
        })
    })
  }
}
