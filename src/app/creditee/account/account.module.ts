import { NgModule } from '@angular/core';
import { AddAccountComponent } from './add/add.component';
import { ListItemComponent } from './list-item/list-item.component';
import { AccountComponent } from './account.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { ThemeModule } from '../../@theme/theme.module';

const COMPONENTS = [
  AccountComponent,
  AddAccountComponent,
  ListItemComponent,
];
const ENTRY_COMPONENTS = [
];

const MODULES = [
  ThemeModule,
  NgxEchartsModule
];

const SERVICES = [
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})

export class AccountModule { }
