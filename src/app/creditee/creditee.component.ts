import { Component } from '@angular/core';
import { CREDITEE_MENU_ITEMS } from './creditee-menu';


@Component({
  selector: 'ngx-creditee',
  styleUrls: ['creditee.component.scss'],
  template: `
    <ngx-sample-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-sample-layout>
  `,
})
export class CrediteeComponent {

  menu = CREDITEE_MENU_ITEMS;
}
