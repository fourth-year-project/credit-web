import { Component, Input, OnInit, Inject } from '@angular/core';

import { NbMenuService, NbSidebarService, NB_WINDOW } from '@nebular/theme';
import { UserData, IUser } from '../../../@core/data/users';
import { AnalyticsService } from '../../../@core/utils';
import { UserService } from '../../../@core/service/user.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  @Input() position = 'normal';

  user: IUser;

  // userMenu = [{ title: 'Profile' }, { title: 'Log Out' }];

  userMenu = [{ title: 'Log Out' }];

  constructor(private sidebarService: NbSidebarService,
              private userService: UserData,
              private nbMenuService: NbMenuService,
              @Inject(NB_WINDOW) private window,
              private router: Router,
              private analyticsService: AnalyticsService) {}

  ngOnInit() {

    this.userService.getUser().subscribe(res =>{
      this.user = res
      this.userService.getCurrentUserById(this.user._uuid).subscribe(usr_res=>{
        console.log("HeaderComponent: " + JSON.stringify(usr_res) ) })
      });


    this.nbMenuService.onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'user-account-context-menu'),
        map(({ item: { title } }) => title),
      )
      .subscribe(title =>{
          if (title == 'Profile')
          {
            this.router.navigate(['/profile'])
          }
          else if (title == 'Log Out')
          {
            this.userService.logout()
          }
          else
          {
            // this.window.alert(`${title} was clicked!`)
            this.window.alert(`Uncaught item clicked!`)
          }
        });
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');

    return false;
  }

  goToHome() {
    this.userService.navigateHome();
  }

  // startSearch() {
  //   this.analyticsService.trackEvent('startSearch');
  // }
}
