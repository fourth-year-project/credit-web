import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'ngx-list-item-placeholder',
  templateUrl: './list-item-placeholder.component.html',
  styleUrls: ['./list-item-placeholder.component.scss']
})
export class ListItemPlaceholderComponent  {
  @HostBinding('attr.aria-label')
  label = 'Loading';
}
