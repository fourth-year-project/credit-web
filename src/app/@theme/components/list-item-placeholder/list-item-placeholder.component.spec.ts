import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListItemPlaceholderComponent } from './list-item-placeholder.component';

describe('ListItemPlaceholderComponent', () => {
  let component: ListItemPlaceholderComponent;
  let fixture: ComponentFixture<ListItemPlaceholderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListItemPlaceholderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListItemPlaceholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
