import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Created by <b> Allen Oyieke - P15/3656/2016 </b></span>
  `,
})
export class FooterComponent {
}
