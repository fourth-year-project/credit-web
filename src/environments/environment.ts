
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiAuthEndpoint: 'http://localhost:8087/',
  apiAuthEndpointUrl:
  {
    getUser:"users/",
    createUser:"users",
    patchUser:"users/",
    getAllUsers:"users/",
    config:"configs",
    creditor:"creditors",
    accountType:"accountType"


  },
  apiEndpoint: 'http://localhost:3000/api/v1/credit/',
  apiEndpointUrl:
  {
    user:"users",
    closeUser:"closeUser",

    organisation:"organisations",
    updateOrganisation:"updateOrganisation",
    deleteOrganisation:"closeOrganisation",

    transactionHistorys:"transactionHistorys",

    watchdog:"watchdogs",
    cancelWatchdog:"cancelWatchdog",
    authorizeWatchdog:"authorizeWatchdog",

    loan:"loans",
    confirmLoan:"confirmLoan",
    confirmLoanPayment:"confirmLoanPayment",
    addExtension:"addLoanExtension",

    account:"accounts",
    updateAccount:"updateAccountStatus",
    closeAccount:"closeAccount"

  },

  accountType:{creditor:"creditor",creditee:"creditee"},
  retryNumber:3,

  companyRegRegex:"^[A-Z]{1,6}[\/][0-9]{4}[\/][0-9]{1,10}$",
  idNumberRegex:"^[0-9]{5,8}$",
  phoneNumberRegex:"^07[0-9]{8,}$",
  telephoneRegex:"^[0-9]{5,10}$",
  accountIDRegex:"^[0-9]{8,30}$",

  nameRegex:"^([a-zA-Z]+[,.]?|[a-zA-Z]+['\\-]?)+$",
  copmanyName:"^([a-zA-Z ]+[,.]?|[a-zA-Z]+['\\-]?)+$",
  emailRegex:'^[a-zA-Z0–9\\_.+\-]+@[a-zA-Z0–9\\-]+.[a-z]+$',
  passwordRegex:'(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,}',
  sentenceRegex:"",

  appRoutes:
  {
    creditee:"creditee/",
    creditor:"creditor/"
  },
  crediteeRoutes:
  {
    dashboard:"dashboard",
    watchdogs:"watchdogs",
    transaction:"transactions",
    addTransaction:"add_transaction",
    loan:"loans",
    addLoan:"add_loan",
    account:"accounts",
    add_account:"add_account"
  },
  creditorRoutes:
  {
    dashboard:"dashboard",
    approval:"approvals",
    clients:"clients",
    clientHistory:"client_history"
  }
};

